#!/usr/bin/env python3

import RPi.GPIO as GPIO 
import socket
import time 
import sys
import os

GPIO.setmode(GPIO.BCM) 
GPIO.setwarnings(False) 

pinEingang=5
GPIO.setup(pinEingang, GPIO.IN, pull_up_down = GPIO.PUD_UP)

coil_A_1_pin = 24 # pink 
coil_A_2_pin = 4 # orange 
coil_B_1_pin = 23 # blau 
coil_B_2_pin = 25 # gelb 
coil2_A_1_pin = 18 # pink 
coil2_A_2_pin = 22 # orange 
coil2_B_1_pin = 17 # blau 
coil2_B_2_pin = 27 # gelb 
 
StepCount = 8 
Seq = list(range(0, StepCount)) 
Seq[0] = [0,1,0,0] 
Seq[1] = [0,1,0,1] 
Seq[2] = [0,0,0,1] 
Seq[3] = [1,0,0,1] 
Seq[4] = [1,0,0,0] 
Seq[5] = [1,0,1,0] 
Seq[6] = [0,0,1,0] 
Seq[7] = [0,1,1,0] 
  
GPIO.setup(coil_A_1_pin, GPIO.OUT) 
GPIO.setup(coil_A_2_pin, GPIO.OUT) 
GPIO.setup(coil_B_1_pin, GPIO.OUT) 
GPIO.setup(coil_B_2_pin, GPIO.OUT) 
 
GPIO.setup(coil2_A_1_pin, GPIO.OUT) 
GPIO.setup(coil2_A_2_pin, GPIO.OUT) 
GPIO.setup(coil2_B_1_pin, GPIO.OUT) 
GPIO.setup(coil2_B_2_pin, GPIO.OUT) 
  
def setStep(w1, w2, w3, w4): 
    GPIO.output(coil2_A_1_pin, w1) 
    GPIO.output(coil2_A_2_pin, w2) 
    GPIO.output(coil2_B_1_pin, w3) 
    GPIO.output(coil2_B_2_pin, w4) 
  
def forward(delay, steps): 
    for i in range(steps): 
        for j in range(StepCount): 
            setStep(Seq[j][0], Seq[j][1], Seq[j][2], Seq[j][3]) 
            time.sleep(delay) 
  
def backward(delay, steps): 
    for i in range(steps): 
        for j in reversed(range(StepCount)): 
            setStep(Seq[j][0], Seq[j][1], Seq[j][2], Seq[j][3]) 
            time.sleep(delay) 

def my_callback(channel):
    print("Klingel erkannt.")
    datei = open('klingel.msg','a+')
    datei.write("Klingel")
    datei.close()
    os.system('mplayer tuerglocke.mp3')
    time.sleep(1)

print("*** MySprechAnlage Server ***")
if (len(sys.argv)<2):
  print("Bitte Host-Übergabeparamter angeben.")
  exit()
else:
  HOST = sys.argv[1]  # Standard loopback interface address (localhost)
  print("Host:"+HOST)

PORT = 50007            # Port to listen on (non-privileged ports are > 1023)
steps=125
delay=5

GPIO.add_event_detect(pinEingang, GPIO.FALLING, callback=my_callback)
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen(1)
    conn, addr = s.accept()
    with conn:
        print('Connected by', addr)
        try:
          while True:
            data = conn.recv(1024)
            if not data:
              pass
            else: 
              print('Received', repr(data))
              if data.decode()=='Schranke zu':
                backward(int(delay) / 1000.0, int(steps))
              if data.decode()=='Schranke auf':
                forward(int(delay) / 1000.0, int(steps))

        except KeyboardInterrupt:
          GPIO.cleanup()
          conn.close()

