#!usr/bin/python3
from tkinter import*
import requests
import zlib
import time 
import PIL.Image
import pyzbar.pyzbar
import base45
import cbor2
import os
from tkinter import messagebox
from datetime import datetime
from socket import *
from PIL import Image, ImageTk

def covid_pruefen():
#  data = pyzbar.pyzbar.decode(PIL.Image.open("covid-horst.png"))
  data = pyzbar.pyzbar.decode(PIL.Image.open(requests.get(urlremote, stream=True).raw))

  print("Len:"+str(len(data)))
  if len(data)>0:
    cert = data[0].data.decode()
    b45data = cert.replace("HC1:", "")
    zlibdata = base45.b45decode(b45data)
    cbordata = zlib.decompress(zlibdata)
    decoded = cbor2.loads(cbordata)
    zert=cbor2.loads(decoded.value[2])
    zertstr=str(zert)
    pos=zertstr.find("'dt':")
    gueltigdatum=zertstr[pos+7:pos+17]
    posstart=zertstr.find("'dob':")
    posende=zertstr.find("'nam':")
    gebdat=zertstr[posstart+8:posende-3]
    posstart=zertstr.find("'gn':")
    posende=zertstr.find("'fnt':")
    vorname=zertstr[posstart+7:posende-3]
    posstart=zertstr.find("'fn':")
    posende=zertstr.find("'gn':")
    nachname=zertstr[posstart+7:posende-3]
    aktdatum=datetime.today().strftime('%Y-%m-%d')
		
    aktdat = datetime.strptime(aktdatum, '%Y-%m-%d')
    gueltdat = datetime.strptime(gueltigdatum, '%Y-%m-%d')
    datdiff = aktdat - gueltdat
    name=vorname+" "+nachname
    print("*************************************************************")
    print("Name       : "+name)
    print("Geb.-Dat.  : "+gebdat)
    print("gültig seit: "+gueltigdatum)
    if datdiff.days>14 and datdiff.days<365:
      print("Das Zertifikat ist gültig!")
      canvas.itemconfig(oval_kreis, fill="white")
      canvas.itemconfig(txt_name, text="Name       : "+name+" ")
      canvas.itemconfig(txt_gebdat, text="Geb.-Dat.  : "+gebdat+" ")
      canvas.itemconfig(txt_gueltdat, text="gültig seit: "+gueltigdatum+" ")
      canvas.itemconfig(txt_meld, text="Im Abfragemodus...")
      result=messagebox.askyesno(title="Abfrage", message="Angaben gültig?")
      if result==True:
        canvas.itemconfig(oval_kreis, fill="green")
        canvas.itemconfig(txt_meld, text="Das Zertifikat ist gültig!")
        strtxt="Schranke auf"
        print(strtxt)
        btxt=strtxt.encode('utf-8')
#NUR zu testzwecken auskommentiert!!
        servsock.send(btxt) 
      else:
        canvas.itemconfig(oval_kreis, fill="red")
        canvas.itemconfig(txt_meld, text="Kein gültiger Nachweis erbracht!")
    else:
      print("UNGÜLTIGES Zertifikat!")
      canvas.itemconfig(txt_meld, text="UNGÜLTIGES Zertifikat!")
      canvas.itemconfig(oval_kreis, fill="red")
  else:
    print("Zertifikat NICHT erkannt!")
    canvas.itemconfig(txt_meld, text="Zertifikat NICHT erkannt!")
    canvas.itemconfig(oval_kreis, fill="red")
    

def Pruefen():
  strtxt="Prüfen"
  print(strtxt)
  strtxt="Pruefen"
  btxt=strtxt.encode('utf-8')
#NUR zu testzwecken auskommentiert!!
  servsock.send( btxt )

#kein receive gewünscht
#  data = servsock.recv( BUFSIZE )
  covid_pruefen()
 

def Schranke_auf():
  strtxt="Schranke auf"
  print(strtxt)
  canvas.itemconfig(txt_meld, text="'"+strtxt+"' gesendet")
  btxt=strtxt.encode('utf-8')
  servsock.send(btxt) 

def Schranke_zu():
  strtxt="Schranke zu"
  print(strtxt) 
  canvas.itemconfig(txt_meld, text="'"+strtxt+"' gesendet")
  btxt=strtxt.encode('utf-8')
  servsock.send(btxt) 

def Angaben_loeschen():
  print("Angaben löschen")
  canvas.itemconfig(oval_kreis, fill="white")
  canvas.itemconfig(txt_name, text="")
  canvas.itemconfig(txt_gebdat, text="")
  canvas.itemconfig(txt_gueltdat, text="")
  canvas.itemconfig(txt_meld, text="Im Wartemodus...")


def KameraAn():
 
  streamremote = Image.open(requests.get(urlremote, stream=True).raw)
  streamtkremote = ImageTk.PhotoImage(image = streamremote)
  labelremotekamera.streamtk = streamtkremote
  labelremotekamera.configure(image=streamtkremote)

  stream_lokal = Image.open(requests.get(url_lokal, stream=True).raw)
  img_lokal = stream_lokal.resize((160,160), Image.ANTIALIAS)
  streamtk_lokal = ImageTk.PhotoImage(image = img_lokal)
  label_lokalkamera.streamtk = streamtk_lokal
  label_lokalkamera.configure(image=streamtk_lokal)
  label_lokalkamera.after(500, KameraAn)
 
def quit():
  global fenster
  print("Programm wurde beendet...")
  servsock.close()
  fenster.destroy()

print("*** MySprechAnlage Client ***")
if (len(sys.argv)<4):
  print("Bitte Übergabeparamter für host, localip und klingelmsg angeben.")
  exit()
else:
  host=sys.argv[1]
  localeip=sys.argv[2]
  klingelmsg=sys.argv[3]
  print("Host:"+host)
  print("localeip:"+localeip)
  print("klingelmsg:"+klingelmsg)

SERVER_PORT = 50007
BUFSIZE = 1024
urlremote="http://"+host+":8080/?action=snapshot"
url_lokal="http://"+localeip+":8080/?action=snapshot"

servsock = socket( AF_INET, SOCK_STREAM)
# Zum Server verbinden
servsock.connect( (host, SERVER_PORT) )

fenster =Tk()
fenster.wm_title('MySprechAnlage')
fenster.wm_attributes("-topmost", 1)
name_var=StringVar()
w=1200
h=500
# get screen width and height
ws = fenster.winfo_screenwidth() # width of the screen
hs = fenster.winfo_screenheight() # height of the screen
# calculate x and y coordinates for the Tk root window
x = (ws/2) - (w/2)
y = (hs/2) - (h/2)
fenster.geometry('%dx%d+%d+%d' % (w, h, x, y))
fenster.configure(bg='white')
canvas = Canvas(fenster, width=w, height=h, bg="white")
canvas.pack()
oval_kreis =canvas.create_oval(340, 10, 480, 150, fill="white")
name=""
gebdat=""
gueltdat=""
txt_name=canvas.create_text(340,210,fill="darkblue",font="Cousine 16",text="Name       : "+name+" ", anchor=W)
txt_gebdat=canvas.create_text(340,230,fill="darkblue",font="Cousine 16",text="Geb.-Dat.  : "+gebdat+" ", anchor=W)
txt_gueltdat=canvas.create_text(340,250,fill="darkblue",font="Cousine 16",text="gültig seit: "+gueltdat+" ", anchor=W)
txt_meld=canvas.create_text(340,272,fill="darkblue",font="Cousine 16",text="Im Wartemodus...", anchor=W)
btnprf = Button(fenster, text='Prüfen', width=20, height=2, bd='2', command=Pruefen)
btnprf.place(x=1000, y=2)
btnauf = Button(fenster, text='Schranke auf', width=20, height=2, bd='2', command=Schranke_auf)
btnauf.place(x=1000, y=62)
btnzu = Button(fenster, text='Schranke zu', width=20, height=2, bd='2', command=Schranke_zu)
btnzu.place(x=1000, y=122)
btnloesch = Button(fenster, text='Angaben löschen', width=20, height=2, bd='2', command=Angaben_loeschen)
btnloesch.place(x=1000, y=182)
btn2 = Button(fenster, text='Beenden', width=20, height=2, bd='2', command=quit)
btn2.place(x=1000, y=440)
label_lokalkamera =Label(fenster, text='Kamerabild', bg='dim gray')
label_lokalkamera.place(x=2, y= 320, width = 160, height = 160)
labelremotekamera =Label(fenster, text='Kamerabild', bg='dim gray')
labelremotekamera.place(x=2, y= 2, width = 300, height = 300)
time_lbl = Label(
    fenster,
    text=time.strftime( "%d.%m.%Y %A %H:%M:%S"),
    bg='#d9d8d7'
    )
time_lbl.place(x=340, y=440)

KameraAn()

while True:
    if os.path.exists(klingelmsg):
      print('Klingel erkannt.')
      os.remove(klingelmsg)
      os.system('mplayer tuerglocke.mp3')
    time.sleep(1)
    time_text=time.strftime("%d/%m/%Y %A %H:%M:%S")
    time_lbl.config(text=time_text)
    fenster.update()

fenster.protocol("WM_DELETE_WINDOW", quit)
fenster.mainloop()
