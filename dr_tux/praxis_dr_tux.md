<h2>Praxis Dr. Tux</h2>

In Anlehnung an die <a href="https://chemnitzer.linux-tage.de/2022/de/addons/praxis">Chemnitzer-Linux-Tage</a> hat nun auch eine Praxis in Köln eröffnet. Im Rahmen der Corona-Vorschriften arbeiten wir sowohl mit Ferndiagnose als auch vor Ort. Die Praxis befindet sich im Gemeindesaal der evang. <a href="https://www.kirche-raderthal.de/kontakt/lageplan/">Philippus-Kirchengemeinde</a> in Köln und findet im Rahmen der monatlich stattfindenden Repair-Initiative statt. Wir geben uns alle Mühe, werden aber leider auch nicht alle Probleme lösen können.
 Wir versuchen, in verschiedenen Fragen zu freier Software (Linux), Python-Programmierung und dem <a href="https://de.wikipedia.org/wiki/Raspberry_Pi">Raspberry Pi</a> mit nützlichen Hinweisen zu dienen, bieten bei kleineren Problemen Unterstützung und sprechen – wenn sonst nichts geht – zumindest Mut zu.

Solltet Ihr Euch bisher nicht an Eure <a href="../projects/projekte.md">Raspberry Pi Projekte</a> getraut haben, so findet Ihr hier fachkundige Unterstützung. Die benötigte Hardware muss aber selber mitgebracht werden.

Bei einer Unterstützung mit <a href="https://de.wikipedia.org/wiki/Jitsi">Jitsi</a> sollte auf eurem Computer entweder ein lauffähiges Betriebssystem mit Browser und Internet oder ein Smartphone mit einer guten Kamera vorhanden sein. Der virtuelle <a href="https://jitsi.fem.tu-ilmenau.de/PraxisDrTuxKoeln">Jitsi-Raum</a> ist nur während der Öffnungszeiten des Repair-Cafés geöffnet und wenn eine Anmeldung vorliegt!

Die Fehlersuche erfolgt auf eigene Gefahr. Für etwaige Schäden können wir keine Haftung übernehmen. Konkret müssen wir daher insbesondere von der Behandlung von Problemen absehen, bei denen die Gefahr besteht, dass hinterher euer Linux nicht mehr hochfährt.

Trotz dieser Einschränkungen könnt ihr uns dennoch gern virtuell oder vor Ort einen Besuch abstatten, vielleicht ist die Lösung doch einfacher als gedacht. Seid aber bitte nicht enttäuscht, wenn unsere „Ärzte“ nein sagen müssen.

Das Angebot ist im Übrigen sehr günstig: Es kostet lediglich ein "Dankeschön".
 
Um den Praxistermin besser planen zu können, wird um eine Anmeldung gebeten:<br>
<a href="https://www.kirche-raderthal.de/angebote/repairinitiative/">Anmeldung Repair Initiate, Praxis Dr. Tux</a><br>
Für die Anmeldung bitte auf den Mont / Termin klicken.
