import time
import os
import RPi.GPIO as GPIO
from keypad import keypad
 
GPIO.setwarnings(False)
 
if __name__ == '__main__':
    kp = keypad(columnCount = 3)
 
    # auf keypad warten
    digit = None
    while digit!='#': 
      digit = None
      while digit == None:
        digit = kp.getKey()
      # Ton ausgeben
      print digit
      if digit == 1:
        os.system("AUDIODEV=hw:1 play ton1a.mp3")
      if digit == 2:
        os.system("AUDIODEV=hw:1 play ton1h.mp3")
      if digit == 3:
        os.system("AUDIODEV=hw:1 play ton2c.mp3")
      if digit == 4:
        os.system("AUDIODEV=hw:1 play ton2d.mp3")
      if digit == 5:
        os.system("AUDIODEV=hw:1 play ton2e.mp3")
      if digit == 6:
        os.system("AUDIODEV=hw:1 play ton2f.mp3")
      if digit == 7:
        os.system("AUDIODEV=hw:1 play ton2g.mp3")
      if digit == 8:
        os.system("AUDIODEV=hw:1 play ton2a.mp3")
      if digit == 9:
        os.system("AUDIODEV=hw:1 play ton2h.mp3")
      if digit == 0:
        os.system("AUDIODEV=hw:1 play ton3c.mp3")
      time.sleep(1)
 
