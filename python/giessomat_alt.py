#!/usr/bin/env python3
import time
import os

from btlewrap import available_backends, BluepyBackend, GatttoolBackend, PygattBackend

from miflora.miflora_poller import MiFloraPoller, \
    MI_CONDUCTIVITY, MI_MOISTURE, MI_LIGHT, MI_TEMPERATURE, MI_BATTERY

print("*** Giessomat 2020 ***")
trocken=False
while 1:
    poller = MiFloraPoller("80:EA:CA:89:18:83", BluepyBackend)
    moisture=(poller.parameter_value(MI_MOISTURE))
    light=(poller.parameter_value(MI_LIGHT))

    print (time.strftime('%H:%M') + " Feuchtigkeit:" + str(moisture) + " % , Licht:" + str(light))
    file = open("giessomat.txt","w")
    file.write(time.strftime('%H:%M') + " Feuchtigkeit:" + str(moisture) + " % , Licht:" + str(light))
    file.close()
    if (moisture<=30):
        print("Pflanze ist zu trocken und wird gegoßen!")
        file = open("giessomat.txt","w")
        file.write(time.strftime('%H:%M') + " Feuchtigkeit:" + str(moisture) + "%\n")
        file.write("Pflanze ist zu trocken und wird gegoßen!")
        file.close()
        if (trocken==False):
          trocken=True
          os.system("sudo sispmctl -o 2")
        os.system("sudo sispmctl -o 1")
        time.sleep(5)
        os.system("sudo sispmctl -f 1")
        print("Pflanze wurde gegoßen.")  
    else:
        if (trocken==True):
          trocken=False
          os.system("sudo sispmctl -f 2")  
    time.sleep(300)            
    
