#!/usr/bin/env python3

#************************
# Author: Horst Meyer
# last Update: 2020-11-23
# URL: https://gitlab.com/horald/raspiprojects/-/blob/master/liesmich.md
#************************

import time
import os

from btlewrap import available_backends, BluepyBackend, GatttoolBackend, PygattBackend

from miflora.miflora_poller import MiFloraPoller, \
    MI_CONDUCTIVITY, MI_MOISTURE, MI_LIGHT, MI_TEMPERATURE, MI_BATTERY

def main():
  print("*** Giessomat 2020 Version 2.0 ***")
  trocken=False
  while 1:
    poller = MiFloraPoller("80:EA:CA:89:18:83", BluepyBackend)
    moisture=(poller.parameter_value(MI_MOISTURE))
    light=(poller.parameter_value(MI_LIGHT))
    battery=(poller.parameter_value(MI_BATTERY))

    zeit=time.strftime('%H:%M')
    print (time.strftime('%H:%M') + " Feuchtigkeit:" + str(moisture) + " % , Licht:" + str(light))
    file = open("giessomat.txt","w")
    file.write(time.strftime('%H:%M') + " Feuchtigkeit:" + str(moisture) + " % , Licht:" + str(light) + " , Batterie: " + str(battery) + " Version 2.0")
    file.close()
    if (moisture<=30):
        if (battery<89):
          print("Pflanze ist zu trocken und muß gegoßen werden! Batterie schwach!")
          file = open("giessomat.txt","w")
          file.write(time.strftime('%H:%M') + " Feuchtigkeit: " + str(moisture) + "% Batterie: " + str(battery) + " Batterie ist schwach (unter 89%)!\n")
          file.write("Bitte Gießvorgang in der Datei 'giessen.txt' mit 'JA' bestätigen.")
          file.close()
          if (trocken==False):
            trocken=True
            os.system("sudo sispmctl -o 2")
          giessdatei = open("gegossen.txt","r")
          if (giessdatei.read()=="JA"):
            giessfile = open("gegossen.txt","w")
            giessfile.write("Pflanze zuletzt am " + time.strftime('%d.%m.%Y') + " um " + time.strftime('%H:%M') + " Uhr bei " + str(moisture) + "% Feuchtigkeit gegossen.")
            giessfile.close()
            os.system("sudo sispmctl -o 1")
            time.sleep(10)
            os.system("sudo sispmctl -f 1")
            giessdatei.close()
            giessdatei = open("gegossen.txt","w")
            giessdatei.write("NEIN")
            giessdatei.close()   
          else:
            giessdatei.close()   
        else:  	
          print("Pflanze ist zu trocken und wird gegoßen!")
          file = open("giessomat.txt","w")
          file.write(time.strftime('%H:%M') + " Feuchtigkeit: " + str(moisture) + "% Batterie: " + str(battery) + "\n")
          if (trocken==False):
            trocken=True
            os.system("sudo sispmctl -o 2")
          if (zeit>'06:00' and zeit<'23:00'):
            giessfile = open("gegossen.txt","w")
            giessfile.write("Pflanze zuletzt am " + time.strftime('%d.%m.%Y') + " um " + time.strftime('%H:%M') + " Uhr bei " + str(moisture) + "% Feuchtigkeit gegossen.")
            giessfile.close()
            os.system("sudo sispmctl -o 1")
            time.sleep(10)
            os.system("sudo sispmctl -f 1")
          else:
            file.write("Sperrstunde! Giesszeit nur zwischen 06:00 und 23:00 Uhr.")
          file.close()
          print("Pflanze wurde gegoßen.")  
    else:
        if (trocken==True):
          trocken=False
          os.system("sudo sispmctl -f 2")  
    time.sleep(300)            
    
if __name__ == "__main__":
  try:
    main()
  except Exception as e:
    logger.exception("Programmabbruch mit Fehler: %s", e)
