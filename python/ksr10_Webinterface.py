import time
from bottle import route, run, post, request

@route('/KSR10_Interface')
def KSR10_Interface():
	return '''
			<h2>KSR10 Webinterface</h2>
			<form action="/ausgabe" method="post">
			Zeit:<input name="sek" type="text" /> <br/>
			Motor:<select name="motor">
				<option value="rumpf">Rumpf (links/rechts)</option>
				<option value="oberarm">Oberarm (links/rechts)</option>
				<option value="unterarm">Unterarm (links/rechts)</option>
				<option value="handgelenk">Handgelenk (hoch/runter)</option>
				<option value="greifer">Greifer (auf/zu)</option>
			</select> <br/> 
			Richtung:<select name="richtung">
				<option value="links">links</option>
				<option value="rechts">rechts</option>
				<option value="hoch">hoch</option>
				<option value="runter">runter</option>
				<option value="auf">auf</option>
				<option value="zu">zu</option>
			</select> <br/> 
			<input value="Abschicken" type="submit" />
			</form>
			'''

@post('/ausgabe')
def get_compl():     
	sek=request.forms.get('sek')   
	motor=request.forms.get('motor')   
	richtung=request.forms.get('richtung')   
	time.sleep(float(sek))
	return ''' 
		<a href="/KSR10_Interface">KSR10 Webinterface</a><br>
		'''+sek+''' Sek. gewartet.<br>
		'''+motor+''' Motor.<br>
		'''+richtung+''' Richtung.
		<meta http-equiv="refresh" content="0; URL=/KSR10_Interface"
   	'''

run(host='localhost', port=8080)