import sys
import time
from random import randint

# importing EAN13 from the python-barcode module
from barcode import EAN13

# importing ImageWriter from python-barcode to generate an image file
from barcode.writer import ImageWriter

from PyQt5.QtWidgets import (
    QApplication,
    QLabel,
    QLineEdit,
    QMessageBox,
    QMainWindow,
    QPushButton,
    QVBoxLayout,
    QWidget,
)

from PyQt5.QtGui import QIcon, QPixmap


class AnotherWindow(QWidget):
    """
    This "window" is a QWidget. If it has no parent,
    it will appear as a free-floating window.
    """

    def __init__(self):
        super().__init__()
        layout = QVBoxLayout()
        self.label = QLabel("Barcode Window ")

        pixmap = QPixmap()
        pixmap.load('bar_code_png.png')
        self.label.clear()
        self.label.update()
        self.label.setPixmap(pixmap)
        self.resize(pixmap.width(),pixmap.height())

        layout.addWidget(self.label)
        self.setLayout(layout)


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.window1 = AnotherWindow()
        self.window2 = AnotherWindow()

        l = QVBoxLayout()
        button1 = QPushButton("Barcode Window")
        button1.clicked.connect(self.toggle_window1)
        l.addWidget(button1)

#        button2 = QPushButton("Push for Window 2")
#        button2.clicked.connect(self.toggle_window2)
#        l.addWidget(button2)

        self.le = QLineEdit(self)
#        le = edit1
#        edit1.move(130, 22)
        l.addWidget(self.le)

        w = QWidget()
        w.setLayout(l)
        self.setCentralWidget(w)

    def toggle_window1(self, checked):
        my_code = EAN13(self.le.text(), writer=ImageWriter())
        my_code.save("bar_code_png")
        time.sleep(1)
#        msgBox = QMessageBox()
#        msgBox.setIcon(QMessageBox.Information)
#        msgBox.setText(self.le.text())
#        msgBox.setWindowTitle("Barcode")
#        msgBox.setStandardButtons(QMessageBox.Ok)
#        returnValue = msgBox.exec()

        if self.window1.isVisible():
            self.window1.hide()

        else:
            self.window1.repaint()
            self.window1.show()

    def toggle_window2(self, checked):
        if self.window2.isVisible():
            self.window2.hide()

        else:
            self.window2.show()


app = QApplication(sys.argv)
w = MainWindow()
w.show()
app.exec()
