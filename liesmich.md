# Raspberry Pi Projekte
Begleitseite zu den von mir privat organisierten Raspberry Pi Workshops<br>
Wer mich unterstützen will, finden unten entsprechende Links.<br>
Alle Projekte stehen unter <a href="projects/projekte.md">Projekte</a><br>

## Vorort-Unterstützung
<s>Seit dem 16.03.2022 hat die <a href="dr_tux/praxis_dr_tux.md">Praxis Dr. Tux</a> auch in Köln geöffnet.</s>*(Wurde wegen mangeldem Interesse leider wieder geschlossen!)*

## Broschüre
* <a href="https://gitlab.com/horald/raspiprojects/-/blob/master/pdfs/Broschuere_Workshop_Raspberry_Pi.pdf">Broschüre zu den Raspberry Pi Workshop's</a>
* <a href="https://gitlab.com/horald/raspiprojects/-/blob/master/pdfs/Raspi_Workshop_Januar_2022.pdf">Präsentation vom Raspberry Pi Workshop in der Stadtbücherei (Stand 15.01.2022)</a>

## Newsletter
Anmeldung unter <a href="https://lists.riseup.net/www/info/raspberry_pi_workshop_koeln">Raspberry Pi Workshop Newsletter</a>

## Soziales Netzwerk
<a href="https://wt.social/wt/raspberry-pi-de">WT.Social Raspberry Pi (DE)</a><br>
<a href="https://troet.cafe/@linuxopensourcerelay@anonsys.net">Mastodon</a>

## Terminübersicht
Wegen mangelnder Teilnehmerzahl ist für alle Termine bei <a href="https://gettogether.community/raspberry-pi-workshop-k%C3%B6ln/">gettogether</a> oder bei <a href="https://www.eventbrite.de/d/germany--k%C3%B6ln/raspberry-pi-workshop/">Eventbrite</a> eine Anmeldung erforderlich.<br>
siehe unter <a href="projects/terminuebersicht.md">Alle Termine</a> (findet nur statt, wenn sich mind. 2 Teilnehmer angemeldet haben!)



## archivierte Termine
siehe unter <a href="projects/termine_archiv.md">alte Termine</a>

## **Ich möchte einen Verein rund um das Thema "Raspberry Pi und Zubehör" gründen und suche noch engagierte Mitstreiter! Einfach bei <a href="https://gettogether.community/raspberry-pi-workshop-k%C3%B6ln/">gettogether</a> anmelden und mir eine Mitteilung schicken. Dieser Link gilt auch für die Anmeldung der Termine, denn für die Teilnahme an den Terminen muss man sich immer anmelden! Der Termin gilt als abgesagt, wenn nicht bis einen Tag vor dem Termin eine Zusage erfolgt ist!**
