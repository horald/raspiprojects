<table>
  <thead>
    <tr>
      <th>Zubehör</th>
      <th>Preis</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href="https://www.conrad.de/de/p/raspberry-pi-b-512-mb-4-x-0-7-ghz-raspberry-pi-1227449.html">Raspberry Pi B+ 512 MB</a></td>
      <td style="text-align: right">54,99</td>
      <td><b><font color=darkred>Erforderlich</font></b></td>
    </tr>
    <tr>
      <td><a href="https://www.conrad.de/de/p/joy-it-rb-case-15-sbc-gehaeuse-passend-fuer-entwicklungskits-raspberry-pi-durchsichtig-1781414.html">Gehäuse</a></td>
      <td style="text-align: right">4,49</td>
      <td><font color=orange>Empfehlung</font></td>
    </tr>
    <tr>
      <td><a href="https://www.conrad.de/de/p/intenso-high-performance-microsdhc-karte-4-gb-class-10-inkl-sd-adapter-417479.html">SD-Speicherkarte</a></td>
      <td style="text-align: right">3,79</td>
      <td><b><font color=darkred>Erforderlich</font></b></td>
    </tr>
    <tr>
      <td><a href="https://www.conrad.de/de/p/joy-it-raspmberry-motorsteuerung-inkl-2-motoren-1274197.html">Motorsteuerung inkl. 2 Motoren</a></td>
      <td style="text-align: right">16,90</td>
      <td><font color=darkblue>Variabel</font></td>
    </tr>
    <tr>
      <td><a href="https://www.conrad.de/de/p/basetech-classic-bs-wc-01-webcam-640-x-480-pixel-klemm-halterung-1616189.html">Webcam</a></td>
      <td style="text-align: right">5,99</td>
      <td><font color=darkblue>Variabel</font></td>
    </tr>
    <tr>
      <td><a href="https://www.conrad.de/de/p/joy-it-lcd-display-1024-x-600-pixel-b-x-h-x-t-165-x-13-x-124-mm-2436027.html">Display</a></td>
      <td style="text-align: right">89,90</td>
      <td><font color=orange>Empfehlung</font></td>
    </tr>
    <tr>
      <td><a href="https://www.conrad.de/de/p/renkforce-mt-1006-funk-tastatur-touchpad-deutsch-qwertz-windows-schwarz-integriertes-touchpad-maustasten-multimedi-1910071.html">Tastatur mit Touchpad</a></td>
      <td style="text-align: right">19,95</td>
      <td><font color=orange>Empfehlung</font></td>
    </tr>
    <tr>
      <td><a href="https://www.conrad.de/de/p/raspberry-pi-cprp020-w-hdmi-kabel-raspberry-pi-1x-hdmi-stecker-1x-hdmi-stecker-2-00-m-weiss-2357063.html">HDMI-Kabel</a></td>
      <td style="text-align: right">5,99</td>
      <td><font color=orange>Empfehlung</font></td>
    </tr>
    <tr>
      <td><a href="https://www.conrad.de/de/p/voltcraft-sps-1000-microusb-steckernetzteil-festspannung-5-v-dc-1000-ma-5-w-518376.html">USB-Netzstecker für Raspberry Pi</a></td>
      <td style="text-align: right">8,99</td>
      <td><font color=orange>Empfehlung</font></td>
    </tr>
    <tr>
      <td><a href="https://www.conrad.de/de/p/voltcraft-sps-1000-microusb-steckernetzteil-festspannung-5-v-dc-1000-ma-5-w-518376.html">USB-Netzstecker für Display</a></td>
      <td style="text-align: right">8,99</td>
      <td><font color=orange>Empfehlung</font></td>
    </tr>
    <tr>
      <td><a href="https://www.bauhaus.info/zierleisten/zierprofil-d-20/p/22812865">Styroporleiste</a></td>
      <td style="text-align: right">1,25</td>
      <td><font color=darkblue>Variabel</font></td>
    </tr>
    <tr>
      <td><a href="https://www.bauhaus.info/reparaturband-isolierband/tesa-isolierband-iso-tape/p/20657547?adb_search=rotes">rotes Klebeband</a></td>
      <td style="text-align: right">1,25</td>
      <td><font color=darkblue>Variabel</font></td>
    </tr>
    <tr>
      <td><a href="https://www.kaufland.de/product/336915641/">USB-Lautsprechern</a></td>
      <td style="text-align: right">6,80</td>
      <td><font color=darkblue>Variabel</font></td>
    </tr>
  </tbody>
</table>