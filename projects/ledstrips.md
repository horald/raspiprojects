<h2>LED-Strips</h2>

<a href="../liesmich.md">Raspberry Pi Projekte</a><br>

RGB LED Streifen gibt es in verschiedenen Ausführen und mit unterschiedlichen Controllern. 
Der APA102 ist eine günstige Alternative. So kann auch der Raspberry Pi APA102 RGB LED Streifen steuern.

Die APA102 LED Strips haben zum einen den Vorteil, dass sie günstig sind und zum anderen, dass es sie in verschiedenen Ausführungen gibt. 
Die Steuerung ist dabei sehr einfach. In diesem kurzen und sehr einfachem Tutorial geht es um die beispielhafte Verkabelung und Steuerung 
eines solchen APA102 LED Streifens am Raspberry Pi.

Folgendes Zubehör wird benötigt:

* <a href="https://www.conrad.de/de/p/raspberry-pi-3-b-1-gb-4-x-1-4-ghz-raspberry-pi-1668026.html">Raspberry Pi</a>
* <a href="">APA102 RGB LED Strip</a>

siehe auch <a href="https://tutorials-raspberrypi.de/raspberry-pi-apa102-led-rgb-strip-python-steuern/">hier</a>

