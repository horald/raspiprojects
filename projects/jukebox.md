<h2>Digitale Jukebox</h2>

<a href="../liesmich.md">Raspberry Pi Projekte</a><br>

Für die Jukebox werden folgende Komponenten gebraucht:<br>

* <a href="https://raspad.sunfounder.com/">Raspberry Pi</a> (z.B. einen Raspad) oder <a href="https://www.berrybase.de/raspberry-pi-co/raspberry-pi/kits/raspberry-pi-3-modell-b-full-starterkit-schwarz?c=2383">Raspberry Pi 3</a> (Full Starterkit)
* <a href="#">HDMI-Monitor</a> (<b>ist beim Raspad schon mit dabei!</b>)
* <a href="https://www.snogard.de/produkte/Logitech/KEYLOGIK400PB/Logitech-K400-Plus-Wireless-Touch-Keyboard-black.html">Bluetooth-Tastatur</a> (zur besseren Bedienung, <b>ist beim neuen Raspad schon mit dabei!</b>)
* <a href="#">USB-Lautsprecher</a> (<b>ist beim neuen Raspad schon mit dabei!</b>)
* <a href="https://www.snogard.de/produkte/Intenso/HDEXT100INMC3/1TB-Intenso-Memory-Center-schwarz.html">externe USB-Festplatte</a> (mit eigener Stromversorgung!)

Ich suche zur Zeit einen neuen Händler, da berrybase gesetzliche Bestimmungen mißachtet! Eine Begründung findet sich <a href="https://de.reclabox.com/beschwerde/223500-sertronics-hamburg-kein-umtausch-falsch-bestellter-ware">hier</a>

Zu erst müssen die Musikstücke gerippt werden. Z.B. mit <a href="https://wiki.ubuntuusers.de/ripperX/">Ripper X</a><br>
Pro Audio-CD sollte man ein eigenes Verzeichnis auf der externen USB-Festplatte anlegen. In diesem Verzeichnis sollte man dann auch eine Cover-Bilddatei abspeichern.

Nun installiert man folgende Programme auf dem Raspberry Pi:<br>

<i>sudo apt-get update</i><br>
<i>sudo apt-get install smplayer</i><br>
<i>sudo apt-get install gcstar</i><br>

Nun können die gewünschten Playlisten in einer Musiksammlung in <a href="https://wiki.ubuntuusers.de/GCstar/">GCStar</a> eingefügt werden. In der Konfiguration muß dann als Player der SMPlayer hinterlegt werden. Danach kann man eine digitale "CD" auswählen und mit Play abspielen. Viel Spaß beim Zuhören.





