<h2>Ein Webserver mit einer PHP-App für die MwSt.-Umrechnung</h2>

<a href="../liesmich.md">Raspberry Pi Projekte</a><br>

Aufgrund der Mehrwertsteuersenkung zeige ich mit diesem Projekt wie man eine Webserver auf dem Raspberry Pi installiert und eine PHP-App installiert, womit man den Gewinn durch die Mehrwertsteuersenkung berechnen kann.
Als erstes müssen wir einen Webserver installieren. Dazu eignet sich der Apache-Webserver recht gut:

<code style="background:gray;color:white;margin:15px">
sudo apt-get install apache2
</code>

Dann brauchen wir noch das PHP-Paket:<br>

<code style="background:gray;color:white;margin:15px">
sudo apt-get install php
</code>

Da die App mit einer sqlite-Datenbank arbeiten, brauchen wir auch noch das Sqlite-Paket für php:<br>

<code style="background:gray;color:white;margin:15px">
sudo apt-get install php-sqlite
</code><br>

Das benötigte PHP-Programm kann <a href="https://gitlab.com/horald/joorgsqlite/-/archive/master/joorgsqlite-master.tar.gz">hier</a> heruntergeladen werden, oder mit git installiert werden:<br>
<code style="background:gray;color:white;margin:15px">
git clone https://gitlab.com/horald/joorgsqlite.git
</code><br>

Wenn man im Browser dann die Adresse "localhost/joorgsqlite" aufruft, findet man einen Menüpunkt "MwStUmrechnung". Dort kann man beide MwSt.-Umrechnungen durchführen. 


