<h2>Offener Themenabend vom 21.04.2020</h2>

<a href="../liesmich.md">Raspberry Pi Projekte</a><br>

Der Termin steht <a href="termine_online.md">hier</a>.

Mögliche Gesprächsthemen:<br>

* Blumengiessen mit dem Raspberry Pi. Bericht über meine erfolgreiche Installation des Giessomats.
* Datum und Uhrzeit-Abfrage aus dem Internet beim Raspberry Pi
* watchdog beim Raspberry Pi
