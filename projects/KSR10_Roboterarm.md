<h2>KSR10 Roboterarm vom 16.04.2019</h2>

<a href="../liesmich.md">Raspberry Pi Projekte</a><br><br>

Vorstellung des Roboterarms KSR10 von der Firma <a href="https://www.velleman.eu/products/view/?id=375310">Vellemann</a>.
Der Roboterarm wird mit einer Handsteuerung ausgeliefert. Zur Ansteuerung muss die passende <a href="https://www.velleman.eu/products/view/?id=436330">USB-Schnittstelle</a> dazugekauft werden.
Vorteil dieses L&ouml;sung ist die g&uuml;nstige Anschaffung von ca. 50 Euro.
Nachteil dieser L&ouml;sung ist, dass der Roboterarm keine Bewegungssensorik hat und somit seine Position nicht bestimmt werden kann. Auch eine Endabschaltung bei zu weiter Aussteuerung ist nicht vorhanden.
Angesteuert werden kann der Roboterarm mit einem Python-Programm. Dazu gibt es eine Open-Source-Bibliothek: <a href="https://sourceforge.net/projects/ksr10usbpython/">KSR10 USB Python controller</a>
Und dann kann mein <a href="https://gitlab.com/horald/raspiprojects/blob/master/python/ksr10_Steuerpult.py">Steuerpult</a> verwendet werden. Im n&auml;chsten Workshop werde ich zeigen wie man den Roboterarm auch mit einem Webinterface steuern kann.
