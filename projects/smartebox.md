<h2>Smarte Box mit elektronischen Schloss</h2>


* <a href="https://www.reichelt.de/entwicklerboards-relais-modul-5-v-srd-05vdc-sl-c-debo-relay-5v-p239148.html?CCOUNTRY=445&LANGUAGE=de&&r=1">Relais-Modul, 5 V</a>
* <a href="https://www.raspberry-buy.de/2-Kanal_Relaisschaltstufe_5V_2xUmschalter.html#menu">2-Kanal Relaismodul 5V</a>
* <a href="https://www.roboter-bausatz.de/detail/index?sArticle=530&sCategory=75&number=RBS10476&nb_arq=1">Elektronisches Türschloss</a>
* <a href="https://www.raspberry-buy.de/Step_Down_Konverter_12V-5V_3A.html">Spannungswandler (DC/DC-Konverter) 12V auf 5V/3A</a>
* <a href="https://www.raspberry-buy.de/USB_Steckernetzteil_12V_3A_offene_Adern_Anschlussmodule.html#menu">Steckernetzteil Raspberry Pi 3 - 12V, 3A, offene Adern</a>

<br>

* <a href="https://www.roboter-bausatz.de/detail/index?sArticle=530&sCategory=75&number=RBS10476&nb_arq=1">Elektronisches Türschloss</a>
* <a href="#">Spannungswandler (DC/DC-Konverter) 12V auf 5V/3A</a>
* <a href="#">Steckernetzteil Raspberry Pi 3 - 12V, 3A, offene Adern</a>

<br>

Hier mein erster Versuch:<br>
<img src="../images/myschrank01.jpg" /><br>
Leider habe ich keine Erfahrung mit dem schneiden von Plexiglas, daher hat das Glas einige Risse.<br><br>
Und hier habe ich den Raspberry Pi mit dem Relais verbunden:<br>
<img src="../images/myschrank02.jpg"><br>
ACHTUNG! Den Strom für den Türöffner erst anschliessen, wenn das Programm einmal gelaufen ist, da das Signal auf High steht.<br>
Mit einem kleinen Mini-Programm kann man dann die Tür öffnen:<br>
<a href="../python/myschrank.py">Türöffner (myschrank.py)</a><br>
<br>

siehe auch: <a href="https://unsere-schule.org/programmieren/python/physical-computing/wir-bauen-ein-elektronisches-schloss/">Wir bauen ein elektronisches Schloss</a>
