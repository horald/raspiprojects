#!/usr/bin/python -u
import RPi.GPIO as GPIO
import pigpio
import time
import os
import subprocess
from waveshare_epd import epd1in54_V2
from PIL import Image,ImageDraw,ImageFont
picdir = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'pic')
libdir = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'lib')
if os.path.exists(libdir):
    sys.path.append(libdir)
 
pi = pigpio.pi()
pi.set_pull_up_down(21,pigpio.PUD_UP)

#GPIO Modus (BOARD / BCM)
GPIO.setmode(GPIO.BCM)
 
#GPIO Pins zuweisen
GPIO_TRIGGER = 18
GPIO_ECHO = 22
 
#Richtung der GPIO-Pins festlegen (IN / OUT)
GPIO.setup(GPIO_TRIGGER, GPIO.OUT)
GPIO.setup(GPIO_ECHO, GPIO.IN)

def play_mp3(path):
    subprocess.Popen(['mpg123', '-q', path]).wait()

def distanz():
  # setze Trigger auf HIGH
  GPIO.output(GPIO_TRIGGER, True)
 
  # setze Trigger nach 0.01ms aus LOW
  time.sleep(0.00001)
  GPIO.output(GPIO_TRIGGER, False)
 
  StartZeit = time.time()
  StopZeit = time.time()
 
  # speichere Startzeit
  while GPIO.input(GPIO_ECHO) == 0:
    StartZeit = time.time()
 
  # speichere Ankunftszeit
  while GPIO.input(GPIO_ECHO) == 1:
    StopZeit = time.time()
 
  # Zeit Differenz zwischen Start und Ankunft
  TimeElapsed = StopZeit - StartZeit
  # mit der Schallgeschwindigkeit (34300 cm/s) multiplizieren
  # und durch 2 teilen, da hin und zurueck
  distanz = (TimeElapsed * 34320) / 2
 
  return distanz

def abstand_messen():
  print("EPaper-Programm wird gestartet...")
  epd = epd1in54_V2.EPD()
  epd.init()
  epd.Clear(0xFF)
  time.sleep(1)
   
  font = ImageFont.truetype(os.path.join(picdir, 'Font.ttc'), 24)
  time_image = Image.new('1', (epd.width, epd.height), 255)
  epd.displayPartBaseImage(epd.getbuffer(time_image))

  minabstand=150
  abstand=distanz()   
  time_draw = ImageDraw.Draw(time_image)
  time_draw.rectangle((10, 10, 120, 50), fill = 255)
  time_draw.text((10, 10), "*** Abstand ***", font = font, fill = 0)
  if abstand<minabstand:
    time_draw.text((10, 34), "ALARM! >"+str(minabstand)+"cm", font = font, fill = 0)
    time_draw.text((10, 58), "%.1f cm" % abstand, font = font, fill = 0)
    time_draw.text((10, 82), "am " + time.strftime('%d.%m.%Y'), font = font, fill = 0)
    time_draw.text((10, 106), "um " + time.strftime('%H:%M') + " Uhr", font = font, fill = 0)
  else:
    time_draw.text((10, 34), "%.1f cm" % abstand, font = font, fill = 0)
    time_draw.text((10, 58), "am " + time.strftime('%d.%m.%Y'), font = font, fill = 0)
    time_draw.text((10, 82), "um " + time.strftime('%H:%M') + " Uhr", font = font, fill = 0)
  newimage = time_image.crop([10, 10, 120, 50])
  time_image.paste(newimage, (10,10))  
  epd.displayPart(epd.getbuffer(time_image))
  print("*** Abstand ***")
  abstand=distanz()   
  print("%.1f cm" % abstand)
  if abstand<minabstand:
    print("Alarm! Abstand unter 150 cm")
    play_mp3('/home/pi/bin/epaper/sirene.mp3')
  print("am " + time.strftime('%d.%m.%Y'))
  print("um " + time.strftime('%H:%M') + " Uhr")

boolmessabstand=True
try: 
  while True:
    if (pi.read(21) == 1):
      # Schalter aus
      boolmessabstand=True
      time.sleep(0.1)
 
    else: 
      if boolmessabstand:
        # Schalter ein
        abstand_messen()
        boolmessabstand=False
      time.sleep(0.1)

except KeyboardInterrupt:    
  GPIO.cleanup()
  exit()
