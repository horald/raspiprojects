## Termine im Büze Ehrenfeld

<a href="../liesmich.md">Raspberry Pi Projekte</a><br><br>

Ort: <a href="http://www.buergerzentrum.info/">Bürgerzentrum Ehrenfeld</a><br>
<br>
## **Zur Zeit finden nur <a href="termine_online.md">Online-Termine</a> statt!**
<br><br>
<b>kommende Termine:</b> die Termine werden vorerst zurückgestellt.<br>
<h3 style="color:red">ACHTUNG! Es finden bis auf weiteres keine Termine mehr im Bürgerzentrum statt, sondern nur noch <a href="termine_online.md">Online</a>!</h3>
<i>Dienstag, 15.12.2020 19:00 bis 20:00 Uhr (nur Online!)</i><br>

<br>
<b>verganene Termine</b><br><br>
Dienstag, <s>17.11.2020</a> 19:00 bis 20:00 Uhr (nur Online!)<br>
Dienstag, <s>20.10.2020</s> 19:00 bis 20:00 Uhr (nur Online!)<br>
Dienstag, <s>15.09.2020</s> 19:00 bis 20:00 Uhr (als Online-Meeting abgehalten)<br>
Dienstag, <s>25.08.2020</s> 19:00 bis 20:00 Uhr (Dieser Termin konnte nur mit 3 Teilnehmern stattfinden, daher hat zeitgleich ein <a href="termine_online.md">Online-Termin</a> stattgefunden.)<br>
Dienstag, <s>21.07.2020</s> 19:00 bis 20:00 Uhr (Dieser Termin konnte nur mit 3 Teilnehmern stattfinden, daher hat zeitgleich ein <a href="termine_online.md">Online-Termin</a> stattgefunden.)<br>
Dienstag, <s>16.06.2020</s> 19:00 bis 20:00 Uhr (Dieser Termin konnte nur mit 3 Teilnehmern stattfinden, daher hat zeitgleich ein <a href="termine_online.md">Online-Termin</a> stattgefunden. Evtl. mit anschließendem Fachsimpeln im Büze-Cafe)<br>
Mittwoch, <s>27.05.2020</s> 19:00 bis 20:00 Uhr (siehe auch <a href="termine_online.md">Online-Termine</a>) Dieser Termin fand nur Online statt, da das Bürgerzentrum für Veranstalter noch geschlossen hat.<br>
Dienstag, <s>19.05.2020</s> 19:00 bis 20:00 Uhr (konnte nicht wahrgenommen werden, da das Bürgerzentrum doch noch zu hatte.)<br>
Dienstag, <s>21.04.2020</s> 19:00 bis 20:00 Uhr (als Online-Meeting abgehalten) <a href="https://yopad.eu/p/RaspberrypiWorkshop-365days">Etherpad</a><br>
Dienstag, <s>17.03.2020</s> 19:00 bis 20:00 Uhr (abgesagt wegen Corona)<br>
Dienstag, <s>18.02.2020</s> 19:00 bis 20:00 Uhr<br>
Dienstag, <s>21.01.2020</s> 19:00 bis 20:00 Uhr<br>
Mittwoch, <s>11.12.2019</s> 19:30 bis 22:00 Uhr<br>
Mittwoch, <s>13.11.2019</s> 19:30 bis 22:00 Uhr<br>
Mittwoch, <s>16.10.2019</s> 19:30 bis 22:00 Uhr<br>
Dienstag, <s>17.09.2019</s> 19:30 bis 22:00 Uhr<br>
Dienstag, <s>20.08.2019</s> 19:30 bis 22:00 Uhr <a href="https://yourpart.eu/p/U7Tp9vjrVb">Etherpad</a><br>
Dienstag, <s>11.06.2019</s> 19:30 bis 22:00 Uhr<br>
Dienstag, <s>14.05.2019</s> 19:30 bis 22:00 Uhr <a href="https://yourpart.eu/p/5p3c98rnNy">Etherpad</a><br>
Dienstag, <s>16.04.2019</s> 19:30 bis 22:00 Uhr<br>

