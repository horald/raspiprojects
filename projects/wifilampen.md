# Ansteuerung von 7 Wifi-Lampen mit dem Raspberry Pi

## Kurzbeschreibung
In diesem Projekt werde ich 7 Wifi-Lampen mit einem Raspberry Pi ansteuern. Die Steuerung erfolgt per Sprachbefehl, was eine interaktive Nutzung der Lampen ermöglicht. Darüber hinaus kann das Licht in verschiedenen Farben eingestellt werden, um eine individuelle Atmosphäre zu schaffen. Als zusätzliches Extra kann mit den Lampen gewürfelt werden – einfach den Sprachbefehl „würfeln“ verwenden.

Schaut euch mein Projekt auf der Maker Fair Ruhr im März 2025 an: [Link](https://www.makerfaire-ruhr.com/)

![Projektbild](../images/makerfaire2025-01.jpg)

*(Erstes Projektbild. Wird noch ersetzt durch ein Bild mit 7 Wifi-Lampen)*

## Benötigtes Zubehör

| Produktname              | Anzahl | Einzel-Preis | Gesamt-Preis | Link zur Quelle           |
|-------------------------|--------|:------------:|:------------:|---------------------------|
| Raspberry Pi 5          | 1      |     55,39 €  |     55,39 €  | [Link](https://buyzero.de/collections/raspberry-pi-5-und-zubehoer/products/raspberry-pi-5-2gb)  |
| Netzstecker             | 1      |      8,89 €  |      8,89 €  | [Link](https://buyzero.de/products/raspberry-pi-15-w-usb-c-netzteil-eu?_pos=6&_sid=b9f11d9bb&_ss=r)  |
| MicroSD-Karte           | 1      |     11,99 €  |     11,99 €  | [Link](https://buyzero.de/products/32-gb-microsd-sandisk?_pos=6&_sid=32d3baef0&_ss=r)  |
| Mikrofon                | 1      |     25,56 €  |     25,56 €  | [Link](https://example.com)  |
| Wifi-Lampe              | 7      |     15,95 €  |    111,65 €  | [Link](https://www.reichelt.de/shelly-duo-rgbw-e27-wi-fi-wlan-lampe-dimmbar-shelly-duo-e27rg-p296857.html?search=shelly+duo)  |
| Sockel für die Lampen   | 7      |     14,00 €  |     98,00 €  | [Link](https://www.tedox.de/tischleuchte-holz-333441.html)  |

**Gesamtkosten:** 311,48 €

**Gesamtkosten:** 131,78 € (bei nur einer Wifi-Lampe)

## Sprachbefehle
- Lampe ein
- Lampe aus
- Lampe Alpha ein
- Lampe Beta ein
- Lampe Gamma ein
- Lampe Delta ein
- Lampe Epsilon ein
- Lampe Zeta ein
- Lampe heller
- Lampe dunkler
- Lampe halb hell
- Lampe ganz hell
- Lampe ganz dunkel
- Lampe rot
- Lampe grün
- Lampe blau
- Lampe gelb
- Würfeln

## Sourcecode
Der Sourcecode für die Steuerung der Wifi-Lampen findest du [hier](../python/sprachsteuerung.py).

