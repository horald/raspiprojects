<h2>RFID-Reader am Beispiel einer automatischen Tür</h2>

<a href="../liesmich.md">Raspberry Pi Projekte</a><br>

Um diesem Workshop praktisch zu folgen, sollte man mindestens folgendes Zubehör haben:<br>

* <a href="https://www.makershop.de/zubehoer/rfid/rfid-rc522-mega-kit/">RFID-Reader</a>

Das Projekt zur Ansteuerung eines Servomotors zur Türsteuerung wurde bereits <a href="servomotor.md">hier</a> vorgestellt.

Die Pinbelegung für den RFID-Reader sieht wie folgt aus:<br>
<img src="../images/rfidpinbelegung.jpg"><br>

<b>Fehlende Angaben werden später noch ergänzt.</b>



