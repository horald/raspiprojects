<h2>Abstand messen per Ultraschall</h2>

<a href="../liesmich.md">Raspberry Pi Projekte</a><br>

Bei diesem Projekt geht es darum, mit dem Raspberry Pi und einem Ultraschallsensor den Abstand zu messen. Dieses eignet sich jetzt in der Corona-Zeit auch dazu den Abstand zu seinem nächsten zu messen und nachzuschauen, ob auch 1,5 Meter oder 2 Meter eingehalten werden. Dies kann man dann per Display anzeigen oder auch ein akkustisches Warnsignal ausgeben. Wenn man es mit einen Raspberry Zero macht und einem Akkupack, so ist es mobil und handlich zu gleich.
Natürlich kann man es auch für andere Zwecke verwenden, z.B. bei einen Roboterauto, dass es nicht gegen ein Hindernis fährt.

Folgendes Zubehör wird benötigt:

* <a href="https://www.conrad.de/de/p/raspberry-pi-zero-wh-512-mb-1-x-1-0-ghz-raspberry-pi-1667360.html">Raspberry Zero</a>
* <a href="https://www.conrad.de/de/p/sandisk-ultra-microsdhc-karte-32-gb-class-10-uhs-i-a1-leistungsstandard-inkl-android-software-inkl-sd-adapter-1595559.html">Speicherkarte</a>
* <a href="https://www.conrad.de/de/p/linker-kit-ultraschall-sensor-1274216.html">Ultraschall Sensor</a>
* <a href="https://www.conrad.de/de/p/weltron-kohleschicht-widerstand-330-axial-bedrahtet-0411-0-5-w-5-1-st-405191.html">330 Ohm Widerstand</a>
* <a href="https://www.conrad.de/de/p/weltron-kohleschicht-widerstand-100-axial-bedrahtet-0411-0-5-w-5-1-st-405132.html">470 Ohm Widerstand</a>
* <a href="https://www.makershop.de/display/e-paper/waveshare-1-54-epaper/">Display</a>
* <a href="#">Minilautsprecher</a> (zur Zeit leider keinen geeigneten Link gefunden.)
* <a href="https://www.conrad.de/de/p/varta-powerpack-2600-powerbank-zusatzakku-li-ion-2600-mah-57959101401-1424859.html">Powerpack</a>
* <a href="https://www.conrad.de/de/p/renkforce-k-1470-strom-kabel-raspberry-pi-arduino-1x-usb-2-0-stecker-a-1x-usb-2-0-stecker-micro-b-1-50-m-schwarz-in-1404046.html">USB Stromschalter</a>
* <a href="https://www.conrad.de/de/p/tru-components-gq12b-a-rd-vandalismusgeschuetzter-drucktaster-48-v-dc-2-a-1-x-aus-ein-ip65-tastend-1-st-701258.html">Ein-/Aus-Schalter</a>


So sieht der Abstandsmesser von oben aus:<br>
<img src="../images/Abstandsmesser_oben.jpg"><br>

So sieht der Abstandsmesser von vorne aus:<br>
<img src="../images/Abstandsmesser_vorne.jpg"><br>

So sieht der Abstandsmesser geöffnet aus:<br>
<img src="../images/Abstandsmesser_offen.jpg"><br>


Für den Abstandsmesser braucht man folgendes Programm: <br> 
<code style="background:gray;color:white;margin:15px">
<a href="display_abstand.py">display_abstand.py</a>
</code><br>
Wie man im Programm sehen kann, habe ich die Taste an den GPIO 21 und den gegenüberliegenden GND angeschlossen.<br>
Damit das Progamm funktioniert, muss natürlich zuerst das epaper-Display konfiguriert werden. Eine Anleitung findet ihr <a href="https://maker-tutorials.com/waveshare-three-color-e-paper-display-am-raspberry-pi-3-anschliessen-eink-driver-hat/">hier</a>

Damit das Programm auch bei Starten läuft, muss es in die Datei /etc/rc.local am Ende vor exit eingebunden werden:<br>
<code style="background:gray;color:white;margin:15px">
/home/pi/bin/epaper/abstand.sh<br>
exit 0                        
</code>

Die Datei abstand.sh sieht wie folgt aus:<br>
<code style="background:gray;color:white;margin:15px">
sudo pigpiod              <br>
cd /home/pi/bin/epaper    <br>
python display_abstand.py   
</code>


Was ich nicht bedacht habe, ist, das der Raspi keine interne Uhr hat. Dadurch das er immer wieder ausgemacht wird, hat er dann keine aktuelle Uhrzeit mehr, es sei denn man holt sich die Zeit beim Booten aus dem Internet. Dies wäre dann nochmal ein eigenes Projekt.

