<h2>Türschild mit Bluetooth-Empfänger</h2>

<a href="../liesmich.md">Raspberry Pi Projekte</a><br><br>

<h3> Anschaffung und Einrichtung </h3>

Für das Türschild wird folgendes benötigt:

* Rasperry Pi Zero
* Display
* USB-Kabel
* Powerpack

Für das Bluetooth-Modul kann folgendes Paket installiert werden:<br>
<code style="background:gray;color:white;margin:15px">
sudo pip3 install pybluez
</code>

Für die Bluetooth-Ansteuerung wird folgendes Modul benötigt:
<a href="btpycom.py">btpycom</a>

<a href="btserver.py">Software für Türschild mit Bluetooth-Empfänger</a>

Um Texte an das Türschild zu schicken, kann folgende Software installiert werden:<br>
<a href="btclient.py">Bluetooth-Client</a>

So kann ein Text auf dem Türschild aussehen:<br>
<img src="../images/Tuerschild1.jpg" alt="Tuerschild Text" width="300" height="400">

Um zu zeigen, ob z.B. das Bad "Frei" oder "Besetzt" ist, kann man dies am elektronischen Türschild umstellen:<br>
<img src="../images/Tuer_frei.png" alt="Tuerschild Text" width="250" height="150">
<img src="../images/Tuer_besetzt.png" alt="Tuerschild Text" width="250" height="150">