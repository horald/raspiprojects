<h2>RoboCar</h2>

<a href="../liesmich.md">Raspberry Pi Projekte</a><br>

Bei diesem Projekt geht es darum, mit dem Raspberry Pi ein Roboterauto zu bauen. Dieses kann über ein softwaregestütztes Steuerpult gesteuert werden. 

Folgendes Zubehör wird benötigt:

* <a href="https://www.conrad.de/de/p/raspberry-pi-3-b-1-gb-4-x-1-4-ghz-raspberry-pi-1668026.html">Raspberry Pi</a>
* <a href="https://www.conrad.de/de/p/sandisk-ultra-microsdhc-karte-32-gb-class-10-uhs-i-a1-leistungsstandard-inkl-android-software-inkl-sd-adapter-1595559.html">Speicherkarte</a>
* <a href="robocar.md">weiteres Zubehör wird noch ergänzt...</a>

So sieht das fertige Roboterauto aus:<br>
<img src="../images/robocar.jpg"><br>


Für das Roboterauto braucht man folgendes Programm: <br> 
<code style="background:gray;color:white;margin:15px">
<a href="motor_Steuerpult.py">motor_Steuerpult.py</a>
</code><br>

Die Beschreibung wird nach dem Online-Meeting im Juni noch aktualisiert...


