<h2>Vorratshaltung</h2>

Um einen besseren Überblick über meinen Vorrat zu erhalten, habe ich angefangen den Vorrat mit meinen Raspberry Pi's zu digitalisieren. Dabei dient ein Raspberry Pi als Datenbank für die Vorratshaltung. Hier habe ich eine Programm (Ein Unterprogramm von meiner Open Source Software <a href="https://gitlab.com/horald/joorgportal">Joorportal</a>.) geschrieben, womit ich den Vorrat einsehen kann.
Und einen zweiten Raspberry Pi habe ich als mobilen Barcodescanner umgebaut:<br>
<img src="../images/Barcodescanner_raspi.jpg" /><br>
Das zugehörige Python-Programm kann ich bei Gelegenheit mal hier einstellen. Nun kann man beim Ein- und Ausgang von Waren in den Vorrat dies bequem mit dem Barcodescanner erfassen und dann in meine Vorratsdatenbank übertragen.
Das Gehäuse für den Barcodescanner wurde selber entworfen und mit meinem 3D-Drucker gedruckt. Je nach Display muss der Druck dann noch angepasst werden.

An Hardware wird folgendes Zubehör benötigt:<br>
Achtung bei den Kabeln. Je nach Raspi-Modell werden andere Netzadapter- und HDMI-Kabel benötigt.<br>
<table>
  <thead>
    <tr>
      <th>Zubehör</th>
      <th>Anzahl</th>
      <th>Preis</th>
      <th>Erfordernis</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href="https://www.conrad.de/de/p/raspberry-pi-b-512-mb-4-x-0-7-ghz-raspberry-pi-1227449.html">Raspberry Pi B+ 512 MB</a></td>
<td>2</td>
      <td style="text-align: right">54,99</td>
      <td><b><font color=darkred>Erforderlich</font></b></td>
    </tr>
    <tr>
      <td><a href="https://www.conrad.de/de/p/joy-it-rb-case-15-sbc-gehaeuse-passend-fuer-entwicklungskits-raspberry-pi-durchsichtig-1781414.html">Gehäuse (gekauft)</a></td>
<td>1</td>
      <td style="text-align: right">4,49</td>
      <td><font color=orange>Empfehlung</font></td>
    </tr>
    <tr>
      <td><a href="">Gehäuse (selbstgedruckt)</a></td>
<td>1</td>
      <td style="text-align: right"></td>
      <td><font color=orange>Empfehlung</font></td>
    </tr>
    <tr>
      <td><a href="https://www.conrad.de/de/p/intenso-high-performance-microsdhc-karte-4-gb-class-10-inkl-sd-adapter-417479.html">SD-Speicherkarte</a></td>
<td>2</td>
      <td style="text-align: right">3,79</td>
      <td><b><font color=darkred>Erforderlich</font></b></td>
    </tr>
    <tr>
      <td><a href="https://www.conrad.de/de/p/joy-it-lcd-display-1024-x-600-pixel-b-x-h-x-t-165-x-13-x-124-mm-2436027.html">Display</a></td>
<td>2</td>
      <td style="text-align: right">89,90</td>
      <td><font color=orange>Empfehlung</font></td>
    </tr>
    <tr>
      <td><a href="https://www.conrad.de/de/p/renkforce-mt-1006-funk-tastatur-touchpad-deutsch-qwertz-windows-schwarz-integriertes-touchpad-maustasten-multimedi-1910071.html">Tastatur mit Touchpad</a></td>
<td>1</td>
      <td style="text-align: right">19,95</td>
      <td><font color=orange>Empfehlung</font></td>
    </tr>
    <tr>
      <td><a href="">Nummer-Tastatur für Scanner</a></td>
<td>1</td>
      <td style="text-align: right">19,95</td>
      <td><font color=orange>Empfehlung</font></td>
    </tr>
    <tr>
      <td><a href="https://www.conrad.de/de/p/raspberry-pi-cprp020-w-hdmi-kabel-raspberry-pi-1x-hdmi-stecker-1x-hdmi-stecker-2-00-m-weiss-2357063.html">HDMI-Kabel</a></td>
<td>2</td>
      <td style="text-align: right">5,99</td>
      <td><font color=orange>Empfehlung</font></td>
    </tr>
    <tr>
      <td><a href="https://www.az-delivery.de/products/akku-powerpack-fur-raspberry-pil">Akku-Pack</a></td>
<td>1</td>
      <td style="text-align: right">19,99</td>
      <td><font color=darkred>Erforderlich</font></td>
    </tr>
<tr>
      <td><a href="https://www.conrad.de/de/p/voltcraft-sps-1000-microusb-steckernetzteil-festspannung-5-v-dc-1000-ma-5-w-518376.html">USB-Netzstecker für Raspberry Pi</a></td>
<td>1</td>
      <td style="text-align: right">8,99</td>
      <td><font color=orange>Empfehlung</font></td>
    </tr>
    <tr>
      <td><a href="https://www.conrad.de/de/p/voltcraft-sps-1000-microusb-steckernetzteil-festspannung-5-v-dc-1000-ma-5-w-518376.html">USB-Netzstecker für Display</a></td>
<td>1</td>
      <td style="text-align: right">8,99</td>
      <td><font color=orange>Empfehlung</font></td>
    </tr>
    <tr>
      <td><a href="">USB-Verbindungskabel für Akku-Pack mit Display u. Raspi</a></td>
<td>2</td>
      <td style="text-align: right">8,99</td>
      <td><font color=orange>Empfehlung</font></td>
    </tr>
  </tbody>
</table>