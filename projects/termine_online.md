## Online-Termine

<a href="../liesmich.md">Raspberry Pi Projekte</a><br>

Hilfe bei Jitsi-Problemen findest Du <a href="jitsi-Probleme.md">hier</a>.

<font color=red>**Eine Anmeldung bei <a href="https://gettogether.community/raspberry-pi-workshop-k%C3%B6ln/">gettogether</a> ist zwingend erforderlich! Wenn nicht bis mindestens einen Tag vor dem Termin eine Zusage erfolgt ist, gilt der Termin als abgesagt!**</font>

### Ort: <b><i>Internet</i></b><br>

<b>kommende Termine</b><br><br>
Dienstag, <s>19.01.2021 19:00 bis 20:00 Uhr</s> Ausgefallen wegen fehlender Teilnehmer! Infos unter <a href="https://jitsi.fem.tu-ilmenau.de/RaspberryPiTreffen">https://jitsi.fem.tu-ilmenau.de/RaspberryPiTreffen</a><br>
<i>Projekt:</i> <a href="raspihandy.md">Raspi als Handy</a><br> 
Dienstag, <s>15.12.2020 19:00 bis 20:00 Uhr</s> ABGESAGT! bei <a href="https://jitsi.fem.tu-ilmenau.de/RaspberryPiWorkshop">https://jitsi.fem.tu-ilmenau.de/RaspberryPiWorkshop</a><br>
<i>Projekt:</i> <a href="jukebox.md">digitale Jukebox</a><br> 

<br>
<b>vergangene Termine</b><br><br>
Dienstag, <s>17.11.2020 19:00 bis 20:00 Uhr</s> bei <a href="https://jitsi.fem.tu-ilmenau.de/RaspberryPiWorkshop">https://jitsi.fem.tu-ilmenau.de/RaspberryPiWorkshop</a><br>
<i>Projekt:</i> <a href="keypad.md">Keypad</a><br> 
Dienstag, <s>20.10.2020 19:00 bis 20:00 Uhr</s> bei <a href="https://jitsi.fem.tu-ilmenau.de/RaspberryPiWorkshop">https://jitsi.fem.tu-ilmenau.de/RaspberryPiWorkshop</a><br>
<i>Projekt:</i> <a href="lichtschranke.md">Lichtschranke</a><br><i>Dieser Termin findet mit begrenzter Teilnehmerzahl auch in <a href="termine_kautsch.md">Hürth</a> statt. (Siehe <a href="https://gettogether.community/events/7294/raspberry-pi-workshop-online-meeting/">hier</a>)</i><br> 
Dienstag, <s>15.09.2020 19:00 bis 20:00 Uhr</s> bei <a href="https://jitsi.fem.tu-ilmenau.de/RaspberryPiWorkshop">https://jitsi.fem.tu-ilmenau.de/RaspberryPiWorkshop</a><br>
<i>Projekt:</i> <a href="funksteckdose.md">Funksteckdose</a><br><i>Dieser Termin fand NUR online statt.</i><br> 
Dienstag, <s>25.08.2020 19:00 bis 20:00 Uhr</s> bei <a href="https://jitsi.fem.tu-ilmenau.de/RaspberryPiWorkshop">https://jitsi.fem.tu-ilmenau.de/RaspberryPiWorkshop</a><br>
<i>Projekt:</i> <a href="rfidreader.md">RFID-Reader am Beispiel einer automatischen Tür</a><br><i>Dieser Termin fand mit geringer Teilnehmerzahl auch im <a href="termine_ehrenfeld.md">Bürgerzentrum Ehrenfeld</a> statt.</i><br> 
Dienstag, <s>21.07.2020 19:00 bis 20:00 Uhr</s> bei <a href="https://jitsi.fem.tu-ilmenau.de/RaspberryPiWorkshop">https://jitsi.fem.tu-ilmenau.de/RaspberryPiWorkshop</a><br>
<i>Projekt:</i> <a href="mwstumrech.md">Ein Webserver mit einer PHP-App für die MwSt.-Umrechnung</a><br><i>Dieser Termin fand mit geringer Teilnehmerzahl auch im <a href="termine_ehrenfeld.md">Bürgerzentrum Ehrenfeld</a> statt.</i><br> 
Dienstag, <s>16.06.2020 19:00 bis 20:00 Uhr</s> bei <a href="https://jitsi.fem.tu-ilmenau.de/RaspberryPiWorkshop">https://jitsi.fem.tu-ilmenau.de/RaspberryPiWorkshop</a> Projekt: <a href="sms.md">SMS senden und empfangen</a><br> 
Mittwoch, <s>27.05.2020 19:00 bis 20:00 Uhr</s> bei <a href="https://jitsi.fem.tu-ilmenau.de/RaspberryPiWorkshop">https://jitsi.fem.tu-ilmenau.de/RaspberryPiWorkshop</a> Projekt: <a href="abstandmessen.md">Abstandsmesser</a><br> 
Dienstag, <s>19.05.2020 19:00 bis 20:00 Uhr</s> (konnte nicht wahrgenommen werden, da das Bürgerzentrum doch noch zu hatte.) <br>
Dienstag, <s>21.04.2020 19:00 bis 20:00 Uhr</s> Online-Pad: <a href="https://yopad.eu/p/RaspberrypiWorkshop-365days">Etherpad</a> Projekt: <a href="opentopics.md">Offener Themenabend</a><br>

