<h2>Servomotor-Ansteuerung vom 18.02.2020</h2>

<a href="../liesmich.md">Raspberry Pi Projekte</a><br><br>

Was wird an Hardware benötigt:<br>

* <a href="https://www.snogard.de/produkte/Raspberry-Pi/SYSTEMRASB214/Raspberry-Pi-3-B%2BBlack-Case-Kit-64GB-microSDHC-1.5m-HDMI-Kabel-schwarz.html">Raspberry Pi und Zubehör</a>
* <a href="https://www.conrad.de/de/p/joy-it-fs90r-fs90r-servomotor-5-v-2110977.html">Servomotor</a>
* <a href="https://www.conrad.de/de/p/joy-it-rb-cb3-25-jumper-kabel-raspberry-pi-banana-pi-arduino-20x-drahtbruecken-buchse-20x-drahtbruecken-buchse-0-25-1182193.html">Kabel</a>

<img src="../images/Servomotor.jpg"><br>

Für die Pinbelegung kann man <a href="https://www.elektronik-kompendium.de/sites/raspberry-pi/1907101.htm">hier</a> nachschaun. An Pin #2 kommt das rote Kabel, an Pin #3 kommt das gelbe Kabel und an Pin #6 oder #9 kommt das braune Kabel.

Folgende Software können wir für die Ansteuerung des Servomotors verwenden:
<img src="../images/ansteuern.png">
