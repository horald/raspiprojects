<h2>SMS</h2>

<a href="../liesmich.md">Raspberry Pi Projekte</a><br>

Sofern man einen Surfstick sein eigen nennt, kann man mit dem Raspberry Pi relativ einfach SMS senden und empfangen.

Zuerst muss man mal den Surfstick einrichten. Dazu schaut man mit lsusb nach, ob der Surfstick erkannt wird:

<code style="background:gray;color:white;margin:15px">
lsusb
</code>

In meinem Fall habe ich einen Surfstick von Huawei im Einsatz:

<pre><code style="background:gray;color:white;margin:15px">
Bus 001 Device 005: ID 12d1:1506 Huawei Technologies Co., Ltd. Modem/Networkcard 
Bus 001 Device 003: ID 0b95:772b ASIX Electronics Corp. AX88772B                 
Bus 001 Device 002: ID 05e3:0608 Genesys Logic, Inc. Hub                         
Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub                   
</code></pre>

Als tool sollte usb-modeswitch installiert werden, da sonst möglicherweise nur die SD-Karte vom Surfstick erkannt wird: <br>
<code style="background:gray;color:white;margin:15px">
sudo apt-get install usb-modeswitch
</code>

Mit der Eingabe des Netzbefehls ifconfig bekommt man dann ein neuen Adapter angezeigt.

<code style="background:gray;color:white;margin:15px">
ifconfig -a
</code>

<pre><code style="background:gray;color:white;margin:15px">
wwan0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500                 
        inet 169.254.62.145  netmask 255.255.0.0  broadcast 169.254.255.255 
        inet6 fe80::83b8:207:d740:7d61  prefixlen 64  scopeid 0x20<link>    
        ether 00:1e:10:1f:00:00  txqueuelen 1000  (Ethernet)                
        RX packets 0  bytes 0 (0.0 B)                                       
        RX errors 0  dropped 0  overruns 0  frame 0                         
        TX packets 2423  bytes 125663 (122.7 KiB)                           
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0          
</code></pre>

<h3> SMS senden </h3>

Für das Senden einer SMS habe ich folgendes Python-Script geschrieben:<br>
<code style="background:gray;color:white;margin:15px">
<a href="sendsms.py">sendsms.py</a>
</code>

zusätzlich muss noch folgends Script angelegt werden:<br>
<code style="background:gray;color:white;margin:15px">
<a href="send2sms.sh">send2sms.sh</a>
</code>

<h3> SMS empfangen </h3>

Für das Empfangen von SMS habe ich folgendes Python-Script geschrieben:<br>
<code style="background:gray;color:white;margin:15px">
<a href="liessms.py">liessms.py</a>
</code>

Ein anderer Weg um die eingegangenen SMS's recht einfach lesen zu können, ist einen Webserver zu installieren. Dazu installiert man am besten apache2:<br>
<code style="background:gray;color:white;margin:15px">
sudo apt-get install apache2
</code>

Danach setzt man einen Softlink auf das SMS-Verzeichnis. Dazu wechselt man zuerst in das Verzeichnis _"/var/www/html"_. Dann gibt man den Link-Befehl ein:<br>
<code style="background:gray;color:white;margin:15px">
ln -s /var/spool/gammu/inbox inbox
</code>

Nun kann man über einen Aufruf im Webbrowser mit der Adresse ___http://ip-adresse-raspi/inbox___ die SMS-Nachrichten sehen.

weiterführende Link:<br>
* <a href="http://raspberry.tips/raspberrypi-tutorials/usb-surfstick-am-raspberry-pi-verwenden-mobiles-internet">USB Surfstick am Raspberry Pi verwenden – Mobiles Internet</a>