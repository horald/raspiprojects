<h2>USB-Steckdosenleiste vom 19.03.2019</h2>

<a href="../liesmich.md">Raspberry Pi Projekte</a><br><br>

Zunächst sorgen Sie dafür das Ihr Betriebssystem auf dem neusten Stand ist:

<code style="background:black;color:white;margin:15px">
sudo apt update<br>
sudo apt upgrade<br>
</code>

Danach brauchen Sie auf dem Raspberry ein Entwicklerpaket für die Programmierung der USB-Schnittstelle. Dazu installieren Sie folgende lib mit root-Rechten:<br>

<code style="background:black;color:white;margin:15px">
sudo apt-get install libusb-dev<br>
</code>

Dann laden Sie die Steuerungssoftware herunter: <a href="http://sourceforge.net/project/showfiles.php?group_id=159834 ">http://sourceforge.net/project/showfiles.php?group_id=159834 </a><br>

Ist die Übertragung beendet, entpacken Sie die Datei mit<br>

<code style="background:black;color:white;margin:15px">
tar xvzf sispmctl.tar.gz (ggfs. müssen Sie die Versionsnummer im Dateinamen angeben)<br>
</code>

und wechseln mit cd sispmct-x.x in das Verzeichnis.<br>

Danach müssen Sie folgende drei Befehle eingeben:

<code style="background:black;color:white;margin:15px">
sudo ./configure<br>
sudo make<br>
sudo make install<br>
</code>

Zur Kontrolle können Sie jetzt sispmctl eingeben. So können Sie die Steckleisten ansteuern:<br>

<code style="background:black;color:white;margin:15px">
sispmctl -o <Nr> (z.B. sispmctl -o 1) zum Einschalten (-o wie on)<br>
sispmctl -f <Nr> (z.B. sispmctl -f 1) zum Ausschalten (-f wie off)<br>
</code>

Da dieser Beitrag noch nicht fertig ist, hilft vielleicht folgender Link weiter: <a href="https://www.sweetpi.de/blog/224/usb-steckdosenleisten-mit-dem-raspberry-pi-schalten">USB-Steckdosenleiste</a> (Link anscheinend nicht mehr aktiv, siehe Alternativ-Link)<br>
Alternativ-Link: <a href="https://www.pcwelt.de/ratgeber/Hausautomation-mit-Open-HAB-Raspberry-Pi-einrichten-9824950.html">Steckdosenleiste auf dem Raspberry einrichten</a>
