## Teilnahmevoraussetzung für die Workshops

<a href="../liesmich.md">Raspberry Pi Projekte</a><br><br>
Folgende Hardware wird für die Teilnahme bei <a href="https://www.kautschservice.de/">Elektro Kautsch</a> benötigt und muss zum Workshop selbst mitgebracht werden:

* Raspberry Pi (alle Modelle außer Pico, da nicht bootfähig)
* Netzstecker/Stromadapter für den Raspberry Pi
* zum Raspberry Pi passende SD-Speicherkarte (mind. 4 GB), bevorzugt Class 10
* Tastatur und Maus mit USB-Anschluss
* HDMI-Monitor u. Kabel oder eigenes Raspberry Pi Display (ggfs. auch ssh-Lösung über Netzwerk an eigenem Laptop möglich. Bei der Einrichtung kann Hilfestellung gegeben werden.)
  


Folgende Hardware wird für die Teilnahme bei der <a href="https://www.stadt-koeln.de/leben-in-koeln/stadtbibliothek/">Stadtbücherei Köln</a> benötigt und muss zum Workshop selbst mitgebracht werden:

* Raspberry Pi (alle Modelle außer Pico, da nicht bootfähig)
* Netzstecker/Stromadapter für den Raspberry Pi
* zum Raspberry Pi passende SD-Speicherkarte (mind. 4 GB), bevorzugt Class 10
* Tastatur und Maus mit USB-Anschluss
* HDMI-Kabel (für Verbindung zum Monitor / Monitore werden leihweise von der Stadtbibliothek gestellt)
