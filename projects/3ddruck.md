<h2>3d-Direktdruck mit OctoPrint</h2>

<a href="../liesmich.md">Raspberry Pi Projekte</a><br><br>

<h3> Anschaffung und Einrichtung </h3>

Für den 3d-Druck mit OctoPrint wird folgendes benötigt:

* 3d-Drucker (z.B. Ender 3)
* Rasperry Pi
* USB-Kabel

Das benötigte Image kann <a href="https://octoprint.org/download/">hier</a> heruntergeladen werden.

<b><i>Dieses Projekt ist noch in der Vorbereitung!</i></b>