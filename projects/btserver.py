# BtEchoServer.py

from btpycom import *

def onStateChanged(state, msg):
    if state == "LISTENING":
        print("Server is listening")
    elif state == "CONNECTED":
        print("Connection established to", msg)
    elif state == "MESSAGE":
        print("Got message:", msg)
        server.sendMessage(msg)
       
serviceName = "EchoServer"
uuid = "0000110a-0000-1000-8000-00805f9b34fb"
server = BTServer(serviceName, stateChanged = onStateChanged, isVerbose = False, uuid = uuid)
