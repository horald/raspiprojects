<h2>Keypad</h2>

<a href="../liesmich.md">Raspberry Pi Projekte</a><br>

Hier geht es um das Auslesen eines Keypads.
Was wird an Hardware benötigt:

* <a href="https://www.snogard.de/produkte/Raspberry-Pi/SYSTEMRASB214/Raspberry-Pi-3-B%2BBlack-Case-Kit-64GB-microSDHC-1.5m-HDMI-Kabel-schwarz.html">Raspberry Pi und Zubehör</a>
* Tastatur und Maus (zur Bedienung des Raspberry Pi's, sollte bereits vorhanden sein)
* HDMI-Monitor oder Fernseher mit HDMI-Eingang
* <a href="#">3x4 Keypad Matrix</a>
* <a href="#">USB-Lautsprecher</a>

Ich suche zur Zeit einen neuen Händler, da berrybase gesetzliche Bestimmungen mißachtet! Eine Begründung findet sich <a href="https://de.reclabox.com/beschwerde/223500-sertronics-hamburg-kein-umtausch-falsch-bestellter-ware">hier</a>


Für den Raspberry Pi gibt es einige Keypad Matrizen, welche relativ einfach angeschlossen und ausgelesen werden können. Ähnlich zum Numpad an einer Tastatur besitzen diese Keypads 3×4 oder 4×4 Tasten. In meinem Beispiel verwende ich 3x4 Tasten. Eingesetzt werden können diese Module für verschiedene Zwecke. In meinem Fall werde ich eine kleine Tonleiter auf die Tasten programmieren. Mithilfe ein paar kleiner Veränderungen kann sogar eine T9 Tastatur erstellt werden, womit Texte wie auf älteren Handys eingetippt werden können. Auf diese Funktion werde ich aber nicht näher eingehen.

<h3>Belegung des Keypads</h3>

<table>
<tr style="background:#88ABC2">
<td>Keypad</td><td>Raspberry Pi</td>
</tr>
<tr style="background:#EBF7F8">
<td>Pin 1</td><td>GPIO 21 (Pin 40 / blau)</td>
</tr>
<tr style="background:#D0E0EB">
<td>Pin 2</td><td>GPIO 20 (Pin 38 / rot)</td>
</tr>
<tr style="background:#EBF7F8">
<td>Pin 3</td><td>GPIO 13 (Pin 33 / schwarz)</td>
</tr>
<tr style="background:#D0E0EB">
<td>Pin 4</td><td>GPIO 16 (Pin 36 / gelb)</td>
</tr>
<tr style="background:#EBF7F8">
<td>Pin 5</td><td>GPIO 5 (Pin 29 / grün)</td>
</tr>
<tr style="background:#D0E0EB">
<td>Pin 6</td><td>GPIO 6 (Pin 31 / violett)</td>
</tr>
<tr style="background:#EBF7F8">
<td>Pin 7</td><td>GPIO 15 (Pin 10 / braun)</td>
</tr>
</table>

<h3>Programmbeispiel zu ansteuern des Keypads</h3>

Um die kleine Tastatur nun benutzen zu können, laden wir uns eine kleine Bibliothek von <a href="https://github.com/rainierez/MatrixKeypad_Python">GitHub</a>. Zuerst erstellen wir einen Ordner:<br>

<code style="background:gray;color:white;margin:15px">
mkdir keypad && cd keypad
</code>

Anschließend laden wir folgende Datei mit wget herunter:

<code style="background:gray;color:white;margin:15px">
wget -O keypad.py https://raw.githubusercontent.com/rainierez/MatrixKeypad_Python/master/matrix_keypad/RPi_GPIO.py
</code>

Achtung! Da ich die Belegung geändert habe, da sie parallel zu einem Display funktionieren sollte, muss dies im Quellcode angepasst werden:

<a href="../python/keypad.py">RPi_GIO.py</a>

Für das Programm brauche ich einige Audiodatei. Diese habe ich mit dem Programm <a href="https://wiki.ubuntuusers.de/Audacity/">Audacity</a> erstellt. Indem man auf Erzeugen/Klang geht, kann man bestimmte Töne nach ihrer Tonhöhe erzeugen. Die Frequenzen können z.B. <a href="https://de.wikipedia.org/wiki/Frequenzen_der_gleichstufigen_Stimmung">hier</a> nachgeschaut werden.

Den Beispielcode kann man hier einsehen:

<a href="../python/tastatur.py">tastatur.py</a>

Viel Spaß beim Programmieren!





