## Termine im Makerspace Bonn

<a href="../liesmich.md">Raspberry Pi Projekte</a><br><br>

Ort: <a href="https://makerspacebonn.de/">Makerspace Bonn</a><br>
<br>
## **Zur Zeit finden nur <a href="termine_online.md">Online-Termine</a> statt!**
<br><br>
<b>kommende Termine:</b> die Termine sind noch in Planung und werden voraussichtlich einmal im Monat samstags von 13:00 Uhr bis 15:00 Uhr stattfinden.<br><br>
