import pigpio
import sys
import time
import RPi.GPIO as GPIO
from Tkinter import *
from tkMessageBox import *

pi = pigpio.pi()
pi.set_pull_up_down(4,pigpio.PUD_UP)
pi.set_pull_up_down(15,pigpio.PUD_UP)

class App:

  LeftForward=38
  LeftBackward=36
  LeftEnable=32
  RightForward=35
  RightBackward=37
  RightEnable=31
  sleeptime=1
  global myspeed
  myspeed=20

  def goodbye(name):
    #print 'Goodbye, %s, it was %s to meet you.' % (name, adjective)
    showinfo('Hinweis', 'Ich sage '+name.get())

	
  def __init__(self, master):
  	 #atexit.register(goodbye, 'tschuess')

    LeftForward=38
    LeftBackward=36
    LeftEnable=32
    RightForward=35
    RightBackward=37
    RightEnable=31
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(LeftForward, GPIO.OUT)
    GPIO.setup(LeftBackward, GPIO.OUT)
    GPIO.setup(LeftEnable, GPIO.OUT)
    GPIO.setup(RightForward, GPIO.OUT)
    GPIO.setup(RightBackward, GPIO.OUT)
    GPIO.setup(RightEnable, GPIO.OUT)

    frame = Frame(master)
    frame.pack()

    self.button = Button(frame, 
                         text="Tschuess", fg="red",
                         command=frame.quit)
    self.button.grid(row=0, column=0, pady = 20)

    self.textlabel = Label(frame, text="Sekunden : ")
    self.textlabel.grid(row=0, column=1, pady = 20)

    self.defval = StringVar(frame, value=str(myspeed))
    self.eingabefeld = Entry(frame, textvariable=self.defval, bd=5, width=40)
    self.eingabefeld.grid(row=1, column=1, pady = 20)

    self.button = Button(frame, 
                         text="Speed", fg="green",
                         command=self.show_speed)
    self.button.grid(row=1, column=3, pady = 20)

    self.slogan = Button(frame,
                         text="Fahre vorwaerts",
                         command=self.fahre_vorwaerts)
    self.slogan.grid(row=2, column=1, pady = 20)

    self.slogan = Button(frame,
                         text="Fahre rueckwaerts",
                         command=self.fahre_rueckwaerts)
    self.slogan.grid(row=5, column=1, pady = 20)

    self.slogan = Button(frame,
                         text="Fahre nach links vorwaerts",
                         command=self.fahre_links_vorwaerts)
    self.slogan.grid(row=3, column=0, pady = 20)
    self.slogan = Button(frame,
                         text="Fahre nach links rueckwaerts",
                         command=self.fahre_links_rueckwaerts)
    self.slogan.grid(row=4, column=0, pady = 20)

    self.slogan = Button(frame,
                         text="Fahre nach rechts vorwaerts",
                         command=self.fahre_rechts_vorwaerts)
    self.slogan.grid(row=3, column=2, pady = 20)
    self.slogan = Button(frame,
                         text="Fahre nach rechts rueckwaerts",
                         command=self.fahre_rechts_rueckwaerts)
    self.slogan.grid(row=4, column=2, pady = 20)

    #self.popupMenu = OptionMenu(frame, self.var, *self.choices) 
    #self.popupMenu.grid(row=2, column=2, pady = 20)       

  def show_speed(self):
      print("Speed:"+self.eingabefeld.get())
      
  def fahre_vorwaerts(self):
      LeftForward=38
      LeftBackward=36
      LeftEnable=32
      RightForward=35
      RightBackward=37
      RightEnable=31
      sleeptime=0.01
      speedl=int(self.eingabefeld.get())
      speedr=int(self.eingabefeld.get())
      print("Fahre vorwaerts mit l="+str(speedl)+" r="+str(speedr))
      GPIO.output(LeftBackward, GPIO.LOW)
      GPIO.output(RightBackward, GPIO.LOW)
      GPIO.output(LeftForward, GPIO.HIGH)
      GPIO.output(RightForward, GPIO.HIGH)
      GPIO.output(LeftEnable, GPIO.HIGH)
      GPIO.output(RightEnable, GPIO.HIGH)
#      time.sleep(sleeptime)
      cl=0
      cr=0
      while cr<speedr or cl<speedl: 
        if pi.read(15) == 1:
          cl = cl + 1
        if pi.read(4) == 1:
        	 cr = cr + 1
        time.sleep(sleeptime) 
        if cl == speedl:  	 
          GPIO.output(LeftForward, GPIO.LOW)
        if cr == speedr:
        	 GPIO.output(RightForward, GPIO.LOW)   
      GPIO.output(LeftForward, GPIO.LOW)
      GPIO.output(RightForward, GPIO.LOW)

  def fahre_rueckwaerts(self):
      LeftForward=38
      LeftBackward=36
      LeftEnable=32
      RightForward=35
      RightBackward=37
      RightEnable=31
      sleeptime=0.01
      speed=int(self.eingabefeld.get())
      print("Fahre rueckwaerts")
      GPIO.output(LeftForward, GPIO.LOW)
      GPIO.output(RightForward, GPIO.LOW)
      GPIO.output(LeftBackward, GPIO.HIGH)
      GPIO.output(RightBackward, GPIO.HIGH)
      GPIO.output(LeftEnable, GPIO.HIGH)
      GPIO.output(RightEnable, GPIO.HIGH)
#      time.sleep(sleeptime)
      cl=0
      cr=0
      while cr<speed or cl<speed: 
        if pi.read(15) == 1:
          cl = cl + 1
        if pi.read(4) == 1:
        	 cr = cr + 1
        time.sleep(sleeptime) 
        if cl == speed:  	 
          GPIO.output(LeftBackward, GPIO.LOW)
        if cr == speed:
        	 GPIO.output(RightBackward, GPIO.LOW)   
      GPIO.output(LeftBackward, GPIO.LOW)
      GPIO.output(RightBackward, GPIO.LOW)

  def fahre_links_vorwaerts(self):
      LeftForward=38
      LeftBackward=36
      LeftEnable=32
      RightForward=35
      RightBackward=37
      RightEnable=31
      sleeptime=0.01
      speed=int(self.eingabefeld.get())
      print("Fahre nach links mit "+str(speed))
      GPIO.output(LeftForward, GPIO.HIGH)
      GPIO.output(LeftBackward, GPIO.LOW)
      GPIO.output(LeftEnable, GPIO.HIGH)
#      time.sleep(sleeptime)
      cs=0
      while cs<speed: 
        if pi.read(15) == 1:
          cs = cs + 1
          time.sleep(sleeptime)
      GPIO.output(LeftForward, GPIO.LOW)

  def fahre_links_rueckwaerts(self):
      LeftForward=38
      LeftBackward=36
      LeftEnable=32
      RightForward=35
      RightBackward=37
      RightEnable=31
      sleeptime=0.01
      speed=int(self.eingabefeld.get())
      print("Fahre nach links mit "+str(speed))
      GPIO.output(LeftForward, GPIO.LOW)
      GPIO.output(LeftBackward, GPIO.HIGH)
      GPIO.output(LeftEnable, GPIO.HIGH)
#      time.sleep(sleeptime)
      cs=0
      while cs<speed: 
        if pi.read(15) == 1:
          cs = cs + 1
          time.sleep(sleeptime)
      GPIO.output(LeftBackward, GPIO.LOW)

  def fahre_rechts_vorwaerts(self):
      LeftForward=38
      LeftBackward=36
      LeftEnable=32
      RightForward=35
      RightBackward=37
      RightEnable=31
      sleeptime=0.01
      speed=int(self.eingabefeld.get())
      print("Fahre nach rechts mit "+str(speed))
      GPIO.output(RightForward, GPIO.HIGH)
      GPIO.output(RightBackward, GPIO.LOW)
      GPIO.output(RightEnable, GPIO.HIGH)
#      time.sleep(sleeptime)
      cr=0 
      while cr<speed: 
        if pi.read(4) == 1:
          cr = cr + 1
          time.sleep(sleeptime)
      print(cr)   
      GPIO.output(RightForward, GPIO.LOW)

  def fahre_rechts_rueckwaerts(self):
      LeftForward=38
      LeftBackward=36
      LeftEnable=32
      RightForward=35
      RightBackward=37
      RightEnable=31
      sleeptime=0.01
      speed=int(self.eingabefeld.get())
      print("Fahre nach rechts mit "+str(speed))
      GPIO.output(RightForward, GPIO.LOW)
      GPIO.output(RightBackward, GPIO.HIGH)
      GPIO.output(RightEnable, GPIO.HIGH)
#      time.sleep(sleeptime)
      cr=0 
      while cr<speed: 
        if pi.read(4) == 1:
          cr = cr + 1
          time.sleep(sleeptime)
      print(cr)   
      GPIO.output(RightBackward, GPIO.LOW)


mode=GPIO.getmode()
root = Tk()
root.title("Robocar Steuerpult")
app = App(root)
root.mainloop()