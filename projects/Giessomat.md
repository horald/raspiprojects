<h2>Giessomat vom 17.03.2020</h2>

<a href="../liesmich.md">Raspberry Pi Projekte</a><br>

Bei diesem Projekt geht es darum, eine Pflanze automatisch mit einem Raspberry Pi zu gießen.<br>
Dabei wird die Bodenfeuchtigkeit der Blumenerde mit einem kapazitiven Feuchtigkeitssensor gemessen.<br>
Wenn dieser dem Raspberry Pi mitteilt, dass die Erde zu trocken ist, dann wird eine Wasserpumpe angestellt und<br>
die Pflanze wird automatisch gegoßen.

So sieht die fertige Installation aus:<br>
<img src="../images/Giessomat1.jpg" alt="Fahrwerk" width="300" height="400">

Folgendes Zubehör wird benötigt:

* <a href="https://www.snogard.de/produkte/Raspberry-Pi/SYSTEMRASB219/Raspberry-Pi-3-B-1GB-Starter-Kit-16GB-microSDHC-1.5m-HDMI-Kabel-schwarz.html">Raspberry Pi und zubehör</a>
* <a href="https://www.amazon.de/Luftfeuchtigkeit-Helligkeit-Temperatur-BLuetooth-Internationale/dp/B01LXOJSWA/ref=sr_1_9?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=miflora&qid=1585686169&sr=8-9">kapazitiver Feuchtigkeitssensor (Flower Care Xiaomi Mi Flora)</a>
* <a href="https://www.conrad.de/de/p/gembird-eg-pms2-programmierbare-ueberspannungsschutz-steckdosenleiste-6fach-schwarz-schutzkontakt-1-st-552507.html">USB-Steckdosenleiste</a> 
* <a href="https://www.amazon.de/gp/r.html?C=3ILR4VQSVD3HI&K=2TLQZTS0J8IGN&M=urn:rtn:msg:20200307153355f07ddadfaad9454d957e6d7e5e70p0eu&R=14RN4XXQ8EQ2&T=C&U=https%3A%2F%2Fwww.amazon.de%2Fdp%2FB01FTL6H8S%2Fref%3Dpe_3044161_185740101_TE_item&H=ARVCMORDADNR29L9KIAPQWO5IBYA&ref_=pe_3044161_185740101_TE_item">Wasserpumpe</a>
* <a href="">Wasserschlauch aus dem Baumarkt</a>

Bei dem Feuchtigkeitssensor habe ich mich für einen Sensor von Miflora entschieden. Dieser hat den Vorteil,<br>
dass die Werte kabellos über eine Bluetooth-Schnittstelle geschickt werden.<br>
Die Software für den Pflanzensensor kann relativ einfach installiert werden:<br>
<code style="background:black;color:white;margin:15px">
sudo pip3 install miflora
</code>

Nun müssen wir zunächst nach dem Sensor suchen. Dazu wird folgender Befehl über die Konsole aufgerufen:<br>
<code style="background:black;color:white;margin:15px">
sudo hcitool lescan
</code>

Nun wird beispielsweise folgendes ausgegeben:<br>
<code style="background:black;color:white;margin:15px">
LE Scan ...<br>
80:EA:CA:89:18:83 (unknown)<br>
80:EA:CA:89:18:83 Flower care<br>
24:FC:E5:B1:39:9E (unknown)<br>
</code>

Die Mac Adresse von "Flower care" werden wir gleich noch brauchen um den Pflanzensensor anzusteuern.<br>
Bevor ich auf die Software des Giessomaten eingehe, wollte ich erst noch erklären, wie die Wasserpumpe angesteuert wird.<br>
Beim Testen der Wasserpumpe ist mir aufgefallen, dass der USB-Anschluß der Wasserpumpe nicht dazu da ist, sie anzusteuern,<br>
sondern lediglich ein 5 Volt Stromanschluß. Darum habe ich mich daran erinnert, dass ich in einen meiner vorherigen Projekte<br>
eine USB-Steckdosenleiste vorgestellt habe. Nun habe ich einfach einen 5 Volt-Stromadapter in die USB-Steckdosenleiste gesteckt.<br>
Da ich hier bis zu 4 Steckdosen steuern kann, habe ich in eine zweite Steckdose eine rote Lampe eingesteckt.<br>
Diese wird nun so angesteuert, dass sie angeht sobald die Pflanze zu wenig Wasser hat, da die Pumpe immer nur einen kurzen Moment<br>
an geht, damit die Pflanze nicht überwässert wird. Der Pflanzensensor wird immer nur alle 5 Minuten abgefragt, weil sonst der<br>
Bluetoothstack überlaufen würde und das Endlosprogramm ungewollt abbrechen würde. Ist die Pflanze wieder feucht genug, sollte nach<br>
spätestens 5 Minuten das rote Licht wieder ausgehen.

Wie man die Software für die USB-Steckdosenleiste einrichtet, habe ich in meinem früheren Projekt bereits beschrieben:<br>
* <a href="USB_Steckdosenleiste.md">USB-Steckdosenleiste</a>

Hier steht die Software für den Giessomaten:<br>
* <a href="../python/giessomat.py">Giessomat-Software</a>

Das Programm ist in einer Endlosschleife geschrieben und läuft rund um die Uhr. Damit man eine Statusrückmeldung erhält, wird alle<br>
5 Minuten eine Statusmeldung in eine Textdatei geschrieben. Diese Meldung kann dann mit folgendem Programm z.B. über eine ssh-Verbindung ausgelesen werden:<br>
* <a href="../python/giessomat_zeigen.py">Anzeigeprogramm</a>

<h3>Update vom 23.11.2020</h3>
Nun habe ich einwenig Langzeiterfahrung und dabei ist mir aufgefallen, dass wenn die Batterie zu schwach wird vom Feuchtigkeitssensor, dann arbeitet er leider<br>
nicht mehr zuverlässig und dann kann es zu einer Überschwemmung kommen. Daher habe ich das verlinkte Giessomat-Programm so geändert, dass wenn der<br>
Batterie-Ladezustand unter 89% fällt, das Giessen der Blume nur noch über eine Status-Datei ermöglicht wird und gleichzeitig vor dem kritischen Batterie-Ladezustand<br>
gewarnt wird, so daß man bei Zeiten die Batterie austauschen kann.<br><br>   

------------------
Alternativer Link:<br>
* <a href="https://tutorials-raspberrypi.de/raspberry-pi-miflora-xiaomi-pflanzensensor-openhab/">Pflanzensensor mit Bluetooth</a>

