import sys
import RPi.GPIO as GPIO
from time import sleep

def SetAngle(angle):
  duty=angle / 18 + 2
  GPIO.output(03, True)
  pwm.ChangeDutyCycle(duty)
  sleep(0.15)
  GPIO.output(03, False)
  pwm.ChangeDutyCycle(0)


print("Servomotor ansteuern")
if len(sys.argv)>1:
  GPIO.setmode(GPIO.BOARD)
  GPIO.setup(03, GPIO.OUT)
  try:
    pwm=GPIO.PWM(03, 50)
    pwm.start(0)
    if sys.argv[1] == "auf":
      file=open("status.txt","w")
      file.write("auf")
      file.close()
      print("Neuer Status:auf")      
      SetAngle(45)
    if sys.argv[1] == "zu":
      file=open("status.txt","w")
      file.write("zu")
      file.close()
      print("Neuer Status:zu")      
      SetAngle(135)
    if sys.argv[1] == "status":
      file=open("status.txt","r")
      print(file.read())
      file.close()
    pwm.stop()
  finally:
    print("clean up")
    GPIO.cleanup()
else:
  print("Bitte Parameter uebergeben.")




