<h2>Gegensprechanlage und Zugangsbeschränkung mit dem Raspberry Pi</h2>

Das Projekt beruht auf meinem älterem Projekte "<a href='https://tube.avensio.de/w/qK5AMeVJmuZTt8kuAD6sZE'>Corona-Schranke</a>" wie ich es in einem Video vorgestellt habe.
Da die Coronabeschränkungen allerdings größtenteils aufgehoben sind, werde ich das Projekt insofern abwandeln, dass man per Webcam eine Gegensprechanlage hat. Es wird eine Klingel geben und man kann mit einem selbsterstellten QRCode oder einer RFID-Card den Zugang erlangen.

Folgende Teile werden benötigt:
<table>
<tr>
<th>Bauteil</th>
<th>Anbieter</th>
<th>Preis</th>
</tr>
<tr>
<td><a href="https://www.rasppishop.de/rpi4-bundle-offiz-teile_8">Raspberry Pi Bundle (inkl. SD-Card, Netzteil u. Gehäuse)</a></td>
<td>rasppishop</td>
<td align="right">98,99 €</td>
</tr>
<tr>
<td><a href="https://www.rasppishop.de/Logitech-Wireless-Touch-Tastatur-K400-Plus-Keyboard">Tastatur wireless mit Touchpad</a></td>
<td>rasppishop</td>
<td align="right">44,99 €</td>
</tr>
<tr>
<td><a href="https://www.rasppishop.de/Raspberry-Pi-Micro-HDMI-zu-HDMI-Kabel-Schwarz-1M">HDMI-Kabel mit Micro-HDMI-Anschluss</a></td>
<td>rasppishop</td>
<td align="right">5,49 €</td>
</tr>
<tr>
<td><a href="https://www.reichelt.de/raspberry-pi-motorsteuerung-inkl-schrittmotoren-uln2803apg-rpi-step-motor2-p176628.html?&trstct=pol_3&nbc=1">Servomotor mit Motorsteuerung</a></td>
<td>reichelt</td>
<td align="right">15,90 €</td>
</tr>
<tr>
<td><a href="https://www.pollin.de/p/joy-it-10-touch-display-im-metallgehaeuse-fuer-raspberry-pi-810824">Monitor, Gehäuse</a> u. Lautsprecher für die Wiedergabe der Klingel</td>
<td>Pollin</td>
<td align="right">157,14 €</td>
</tr>
<tr>
<td><a href="https://www.reichelt.de/webcam-logitech-c270-hd-logitech-hd-c270-p100943.html?&trstct=pos_0&nbc=1">Webcam</a></td>
<td>reichelt</td>
<td align="right">24,95 €</td>
</tr>
<tr>
<td><a href="https://www.reichelt.de/entwicklerboards-rfid-modul-nxp-mfrc-522-debo-rfid-rc522-p192147.html?&trstct=pos_4&nbc=1">RFID-Lesegerät</a></td>
<td>reichelt</td>
<td align="right">10,40 €</td>
</tr>
<tr>
<td><a href="https://www.bauhaus.info/tuerklingeln/heidemann-klingeltaster-unbeleuchtet/p/22471141">Klingeltaster</a></td>
<td>Bauhaus</td>
<td align="right">5,40 €</td>
</tr>
<tr>
<td><a href="https://funduinoshop.com/elektronische-module/wireless-iot/rfid-nfc/rfid-tag-scheckkartenformat-125khz">RFID TAG – Scheckkarten</a></td>
<td>Funduinoshop</td>
<td align="right">0,40 €</td>
</tr>
<tr>
<td><a href="https://www.bauhaus.info/moebelbauholz/massivholzbrett-tiroler-fichte/p/27190593">Holzbrett mit Folie beklebt für die Schranke, Raspi, Monitor und anders Zubehör</a></td>
<td>Bauhaus</td>
<td align="right">8,50 €</td>
</tr>
<tr>
<td> </td>
<td> </td>
<td align="right">--------------</td>
</tr>
<tr>
<td> </td>
<td> </td>
<td align="right">372,16 €</td>
</tr>
</table>

Die Einkaufsliste ist nur eine Empfehlung und die Teile können natürlich auch woanders eingekauft werden.<br>
Die Gegenseite erfolgt auf einem normalen Linux-Rechner oder Linux-Laptop. Der Raspberry Pi und der Linux-Rechner werden über ein normales Ethernet-Netzwerk verbunden.<br>

Um die Webcam zu streamen muss folgende Software auf dem Raspberry Pi und auf dem Linux-Rechner installiert und gestartet werden:<br>
<a href="http://sourceforge.net/projects/mjpg-streamer/">mjpg-streamer</a><br>
Für den Start habe ich ein Skript (z.B. <i>mjpg-streamer.sh</i>) erstellt, dass wie folgt aussehen kann:<br>
<code style="background:black;color:white;margin:15px">
mjpg_streamer -i "input_uvc.so -d /dev/video0" -o "output_http.so -l 192.168.0.179"
</code><br>
Wie gesagt, das Skript muss sowohl auf dem Raspberry Pi als auch auf dem Linux-Rechner laufen und dort muss die passende IP-Adresse angegeben werden.<br>

Die Webcams werden auf dem Raspberry Pi mit einem PHP-Script <a href="../src/mysprechanlage.php">mysprechanlage.php</a> angezeigt und mit dem Skript <i>showcams.sh</i> wie folgt gestartet:<br>
<code style="background:black;color:white;margin:15px">
firefox http://192.168.0.179/mysprechanlage/mysprechanlage.php
</code><br>

Für die Zugangsschranke auf dem Raspberry Pi wird folgende Software benötigt:<br>
<a href="../src/mysprechanlage_server.py">Zugangssoftware</a> (mysprechanlage_server.py)<br>
Danach wird einfach das Script wie folgt gestartet:<br>
<code style="background:black;color:white;margin:15px">
./mysprechanlage_server.sh 
</code><br>
Wichtig ist, dass im Script die Server-IP-Adresse mit übergeben wird. Der Inhalt kann dann wie folgt aussehen:<br>
<code style="background:black;color:white;margin:15px">
python3 mysprechanlage_server.py 192.168.0.179
</code><br>

Für die Gegenseite auf dem Linxu-Rechner wird folgende Software (<i>mysprechanlage.py</i>) benötigt:<br>
<a href="../src/mysprechanlage.py">Gegensprechanlagensoftware</a><br>
Auch hier habe ich ein Skript geschrieben, wo die nötigen Übergabenparameter angegeben werden:<br>
<code style="background:black;color:white;margin:15px">
./mysprechanlage.sh 
</code><br>
Der Inhalt sieht dann wie folgt aus:<br>
<code style="background:black;color:white;margin:15px">
python3 mysprechanlage.py 192.168.0.179 192.168.0.156 /media/raspi-abstand/home/pi/bin/MySprechAnlage/klingel.msg
</code><br>
Da ich die Mitteilung, das es geklingelt hat nicht über den Socket übertragen bekommen habe, habe ich einen Trick angewendet und der Linux-Rechner wird mit dem Raspberry Pi gemountet. Wenn es jetzt am Raspberry Pi klingelt, wird eine Datei "klingel.msg" erzeugt. Auf dem Linux-Rechner wird nun abgefragt, ob es diese Datei gibt und wenn ja dann hört man die Klingel. Anschließend wird die "Klingel"-Datei wieder gelöscht, damit die Klingel nicht permanent läutet :-)<br>
Der Klingelschalter ist am Raspberry Pi mit dem GPIO 5 und dem gegenüberliegenden GND verbunden:<br>
Die Audioanbindung habe ich leider nicht hinbekommen. Ich wollte es mit <a href="https://wiki.ubuntuusers.de/Mumble/">mumble</a> und einem eigenen <a href="https://wiki.natenom.de/mumble/benutzerhandbuch/murmur/mumble-server-installieren/debian.html">mumble-Server</a> machen, aber ich habe die Sound-Einstellung leider nicht störungsfrei hinbekommen. Bin für Tipps dankbar.

<i>Da mein altes RFID-Lesegerät leider nicht mehr funktioniert, habe ich ein neues bestellt. Daher fehlt dieser Part hier auch noch...</i>

Viel Spaß mit dem Projekt!

<b><i>Über konstruktive Rückmeldungen würde ich mich freuen!</i></b>
