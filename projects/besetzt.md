<h2>Besetzt</h2>

<a href="../liesmich.md">Raspberry Pi Projekte</a><br>

Ist das Bad besetzt oder frei? Mit einem Bewegungsmelder können Sie das abfragen und außen an der Badezimmertür automatisch anzeigen, ob jemand im Bad ist oder nicht.
Was wird an Hardware benötigt:

* <a href="">Bewegungsmelder</a>
* <a href="">Jumper Kabel (Female – Female)</a>
* <a href="">epaper farbe</a>

<h3>Belegung des Bewegungsmelders</h3>

<table>
<tr style="background:#88ABC2">
<td>Sensor</td><td>Raspberry Pi</td>
</tr>
<tr style="background:#EBF7F8">
<td>Pin 1</td><td>+5 V (Pin 2 / rot)</td>
</tr>
<tr style="background:#D0E0EB">
<td>Pin 2</td><td>GND (Pin 9 / schwarz)</td>
</tr>
<tr style="background:#EBF7F8">
<td>Pin 3</td><td>GPIO 23 (Pin 16 / gelb)</td>
</tr>
</table>

<img src="../images/pinbelegung-bewegungsmelder.png"><br>


Hier eine alternative Dokumentation:

<a href="https://tutorials-raspberrypi.de/raspberry-pi-bewegungsmelder-sensor-pir/">Raspberry Pi Bewegungsmelder PIR anschließen und steuern</a>






