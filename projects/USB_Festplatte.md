<h2>USB-Festplatte vom 17.09.2019</h2>

<a href="../liesmich.md">Raspberry Pi Projekte</a><br><br>

USB-Festplatten und USB-Stick können in der Regel relativ problemlos am Rasperry Pi angeschlossen werden. Voraussetzung ist, dass man das Rasbian-Betriebssystem verwendet. Sofern man nur reine Linux-Dateien austauscht, ist schon alles vorinstalliert. Wenn man auch andere Dateisysteme verwenden will muss man noch andere Pakete nachinstallieren. Bei externen Festplatten sollte man darauf achten, dass sie möglichst eine eigene Stromversorgung haben, da der Raspberry Pi über die USB-Schnittstelle nur die Leistung des USB-Netzadapters weitergeben kann, die in der Regel auch nicht so hoch ausfällt.

Für weitere Informationen siehe folgende Links: <br>

* <a href="https://jankarres.de/2013/01/raspberry-pi-usb-stick-und-usb-festplatte-einbinden/">Externe Festplatte (USB)</a>
* <a href="https://www.randombrick.de/raspberry-pi-usb-stick-und-usb-festplatte-einbinden/">Festplatte mounten (USB)</a>