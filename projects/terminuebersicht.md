## Übersicht aller Termine

<a href="../liesmich.md">Raspberry Pi Projekte</a><br><br>
Teilnahmevoraussetzung für die Workshops: <a href="vorraussetzung.md">siehe hier</a><br><br>
<b>kommende Termine:</b><br>
<i>Donnerstag, 07.11.2024 19:00 bis 21:00 Uhr</i><br>
Ort: <a href="https://klugev.de/">Klug e.V.</a> Thema: <a href="wifilampen.md">Wifi-Lampen per Sprache steuern</a><br>
Die <a href="https://www.eventbrite.de/e/raspberry-pi-workshop-tickets-1039862024307?aff=oddtdtcreator">Anmeldung</a> zu diesem Workshop erfolgt über EventBrite.
<br><br><br>
<b>vergangene Termine:</b><br>
<s><i>Donnerstag, 07.12.2023 19:00 bis 21:00 Uhr</i></s><br>
Ort: <a href="https://klugev.de/">Klug e.V.</a> Thema: <a href="#">Offener Themenabend</a><br>
<font color="darkred">Diesen Workshop ist leider schon ausgebucht. Für Januar 2024 ist ein neuer Workshop geplant.</font>
<br><br><br>
<s><i>Donnerstag, 04.01.2024 19:00 bis 21:00 Uhr</i></s><br>
Ort: <a href="https://klugev.de/">Klug e.V.</a> Thema: <a href="#">(noch nicht bekannt.)</a><br>
Die <a href="https://www.eventbrite.de/e/raspberry-pi-workshop-tickets-770928587157?aff=ebdssbdestsearch">Anmeldung</a> zu diesem Workshop erfolgt über EventBrite.
<br><br>
<s><i>Samstag, 15.01.2022 10:00 bis 12:00 Uhr</i></s><br>
Ort: <a href="https://www.stadt-koeln.de/leben-in-koeln/stadtbibliothek/">Stadtbücherei Köln</a> Thema: <a href="https://stadt-koeln.easy2book.de/einfuehrung-in-den-raspberry-pi-event-1253">Einführung in den Raspberry Pi</a><br>
<font color="darkred">Für diesen Workshop ist eine Anmeldung über das Buchungssystem der Stadtbücherei Köln erforderlich: <a href="https://stadt-koeln.easy2book.de/einfuehrung-in-den-raspberry-pi-event-1253">Anmeldeportal und Workshop-Info</a></font>
<br><br><br>
<s><i>Dienstag, 20.07.2021 19:00 bis 20:00 Uhr</i></s> (Abgesagt wegen mangelnder Teilnehmer)<br>
Ort: <a href="https://www.kautschservice.de/">Elektro Kautsch</a> Thema: <a href="gpio_led.md">GPIO am Beispiel einer RGB-LED</a> <b><font color="darkred">(Abhängig von der Anzahl der Teilnehmer werden ggfs. weitere Workshops geplant!)</font></b>
<br><br>
<s><i>Samstag, 28.08.2021 10:00 bis 12:00 Uhr</i></s> (Abgesagt wegen mangelnder Teilnehmer)<br>
Ort: <a href="https://www.stadt-koeln.de/leben-in-koeln/stadtbibliothek/">Stadtbücherei Köln</a> Thema: <a href="https://stadt-koeln.easy2book.de/einfuehrung-in-den-raspberry-pi-event-1114">Einführung in den Raspberry Pi</a><br>
<font color="darkred">Für diesen Workshop ist eine Anmeldung über das Buchungssystem der Stadtbücherei Köln erforderlich: <a href="https://stadt-koeln.easy2book.de/einfuehrung-in-den-raspberry-pi-event-1114">Anmeldeportal und Workshop-Info</a></font>
<br><br>
<s><i>Samstag, 25.09.2021 10:00 bis 12:00 Uhr</i></s> (Abgesagt wegen mangelnder Teilnehmer)<br>
Ort: <a href="https://www.stadt-koeln.de/leben-in-koeln/stadtbibliothek/">Stadtbücherei Köln</a> Thema: <a href="jukebox.md">Raspberry Pi als digitale Musicbox</a><br>
<font color="darkred">Für diesen Workshop ist eine Anmeldung über das Buchungssystem der Stadtbücherei Köln erforderlich: <a href="https://stadt-koeln.easy2book.de/raspberry-pi-als-digitale-musicbox-event-1115">Anmeldeportal und Workshop-Info</a></font>
<br><br>
