## Termine bei der Fa. Elektro Kautsch in Hürth

<a href="../liesmich.md">Raspberry Pi Projekte</a><br><br>

Ort: <a href="https://www.kautschservice.de/">Elektro Kautsch</a><br>
<br>
## **Zur Zeit finden nur <a href="termine_online.md">Online-Termine</a> statt!**
<br><br>
<b>kommende Termine:</b><br>
<i>Dienstag, 15.12.2020 19:00 bis 20:00 Uhr</i><br>

<br>
<b>verganene Termine</b><br><br>
<i>Dienstag, 17.11.2020 19:00 bis 20:00 Uhr</i> (Der Termin wurde abgesagt und fand nur Online statt.)<br>
<i>Dienstag, 20.10.2020 19:00 bis 20:00 Uhr</i> (Der Termin wurde abgesagt und fand nur Online statt.)<br>

