# Alle archivierten Termine

## Online-Termine
Ort: <a href="https://de.wikipedia.org/wiki/Internet">Internet</a> (wegen mangelnder Teilnehmerzahl bitte Termine bei <a href="https://gettogether.community/raspberry-pi-workshop-k%C3%B6ln/">gettogether</a> anfragen.)<br>
siehe unter <a href="projects/termine_online.md">Termine Online</a> (findet nur statt, wenn sich mind. 2 Teilnehmer angemeldet haben!)

## Termine bei der Stadtbücherei Köln (im Rahmen von Makerspace)
Ort: <a href="https://www.stadt-koeln.de/leben-in-koeln/stadtbibliothek/index.html">Stadtbücherei Köln</a> für die Anmeldung der Termine bei der Stadtbücherei bitte das Buchungssystem der Stadtbücherei verwenden.<br>
siehe unter <a href="#">Termine Stadtbibliothek</a> (Termine stehen noch nicht fest, werden aber demnächst veröffentlicht.)

## Termine bei der Fa. Elektro Kautsch in Hürth
Ort: <a href="https://www.kautschservice.de/">Elektro Kautsch</a><br>
siehe unter <a href="projects/termine_kautsch.md">Termine Fa. Kautsch</a> (wegen Corona zur Zeit noch geschlossen.)


## Termine im Büze Ehrenfeld
Ort: <a href="#">Bürgerzentrum Ehrenfeld</a> Das Bürgerzentrum unterstützt mein digitales und ehrenamtliches Engagement für die Bürger nicht!<br>
siehe unter <a href="projects/termine_ehrenfeld.md">Termine Ehrenfeld</a> (veraltet!)

## Termine im Makerspace Bonn
Ort: <a href="https://makerspacebonn.de/">Makerspace Bonn</a><br>
siehe unter <a href="projects/termine_bonn.md">Termine Bonn</a> (zur Zeit keine weiteren Termine geplant!)