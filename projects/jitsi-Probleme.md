## Jitsi-Probleme

<a href="../liesmich.md">Raspberry Pi Projekte</a><br>

Hier habe ich eine kleine Link-Sammlung zusammengestellt, wo man Hilfe bei Jitsi-Problemen findet:

* <a href="https://www.kuketz-blog.de/jitsi-meet-erste-hilfe-bei-problemen/">Kuketz-Hilfe</a>
* <a href="https://scheible.it/jitsi-meet-tipps-tricks/">Tipps und Tricks mit Jitsi Meet</a>
* <a href="https://geisler-design.de/jitsi-kein-ton">Jitsi: kein Ton</a>

Leider unterstützt Jitsi nicht alle Browser, gerade der Firefox-Browser wird nicht in allen Versionen unterstützt und es kann zu Problemen bei der Bildübertragung kommen (Scharzes Bild, oder Dummy-User-Bild), daher sind folgende Browser zu empfehlen:

* <a href="https://www.chip.de/downloads/Ungoogled-Chromium_101325413.html">Ungoogled Chromium</a>
* <a href="https://iridiumbrowser.de/">Iridium</a>
* <a href="https://www.chip.de/downloads/Jitsi_56854700.html">Jitsi-App</a> 

Die Jitsi-App kann hinter einem Proxy wie folgt genutzt werden:<br>
<code style="background:gray;color:white;margin:15px">
./jitsi-meet-x86_64.AppImage -proxy-server=192.168.0.123:3128
</code><br>
Ich habe diesen Aufruf bisher aber leider noch nicht testen können...