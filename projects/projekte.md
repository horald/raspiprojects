<h2>Projekte</h2>

<a href="../liesmich.md">Raspberry Pi Projekte</a><br><br>

Folgendes aktuelle Projekt kann bei der Maker Faire Ruhr im März 2025 angeschaut werden:<br>

* <a href="wifilampen.md">Sprachsteuerung von Wifi-Lampen mit dem Raspberry Pi</a>


Folgende Projekte als Video anschauen:<br>

* <a href="https://tube.avensio.de/w/qK5AMeVJmuZTt8kuAD6sZE">Corona-Schranke</a>

Folgende Projekte sind in Vorbereitung:<br>

* <a href="smartebox.md">Smarte Box mit elektronischem Schloss</a>
* <a href="vorrat.md">Vorratshaltung</a> (Termin auf Anfrage!)
* <a href="gegensprechanlage.md">Gegensprechanlage und Zugangsbeschränkung</a> (wird am 10.-11.09.2022 auf der <a href="https://maker-faire.de/hannover/">Maker Faire in Hannover</a> vorgestellt)
* <a href="3ddruck.md">3d-Direktdruck mit OctoPrint</a> (Termin auf Anfrage!)
* <a href="tuerschild.md">Türschild mit Bluetooth-Empfänger</a> (Termin auf Anfrage!)
* <a href="email_versenden.md">EMail versenden</a> (Termin auf Anfrage!)
* <a href="USB_Festplatte.md">Externe Festplatte (USB)</a> (Termin auf Anfrage!)

Folgende Projektideen stehen noch zum Angebot:<br>

* <a href="#">Kamerabild mit Taste auslösen</a> (Fotoapparat per Raspi bauen)
* <a href="#">Blättern per Fußschalter</a> 
* <a href="https://www.raspberry-pi-geek.de/ausgaben/rpg/2018/02/raspi-via-netzwerk-booten/">Booten via Netzwerk</a> 
* <a href="#">Gegenstände orten mit dem Raspberry Pi</a> (Indoor-Tracking)
* <a href="#">Fernsteuerung eines Roboterautos</a> (Beides mit einem Raspberry Pi umgesetzt)
* <a href="#">Fernsteuerung eines Roboterarms</a> (Beides mit einem Raspberry Pi umgesetzt)
* <a href="https://www.einplatinencomputer.com/raspberry-pi-media-center-distributionen-im-ueberblick/">Mediacenter</a> (Musik/Video/upnp)
* <a href="https://www.kompf.de/cplus/emeocv.html">Texterkennung mit einer Webcam z.B. f&uuml;r den Stromz&auml;hler</a>
* <a href="https://tutorials-raspberrypi.de/raspberry-pi-ueberwachungskamera-livestream-einrichten/">Webcam ins Netzwerk / Internet einbinden</a>
* <a href="https://www.raspberrypi.org/blog/send-loved-ones-audio-messages/">Audio-Nachricht</a>
* <a href="http://wiki.lug-saar.de/projekte/internetradio">Webradio</a>
* <a href="https://www.makeuseof.com/tag/play-theme-tune-enter-room-raspberry-pi/">Begr&uuml;&szlig;ungssong</a>
* <a href="https://www.linuxmuster.net/wiki/anwenderwiki:infoboard:raspberry-pi">digitales Schwarzes Brett</a>
* <a href="https://pi-buch.info/bluetooth-lautsprecher-verwenden/">Bluetooth-Speaker anbinden</a>
* <a href="https://raspberry.tips/raspberrypi-tutorials/bewegungsmelder-am-raspberry-pi-auslesen">Monitor mit Bewegungsmelder steuern</a>
* <a href="https://www.tutonaut.de/anleitung-raspberry-pi-via-vnc-fernsteuern/">Remote-Desktop-Verbindung</a>
* <a href="https://hackmd.okfn.de/buchscanner_calldoku?view">Buchscanner</a>
* <a href="https://www.golem.de/news/geigerzaehler-radioaktivitaet-messen-mit-dem-raspberry-pi-1904-140654.html">Geigerz&auml;hler</a>
* <a href="https://tutorials-raspberrypi.de/raspberry-pi-gsm-modul-mobiles-internet/">Raspi als Handy</a>
* <a href="https://www.einplatinencomputer.com/raspberry-pi-media-center-distributionen-im-ueberblick/">Mediacenter</a> (<s>Februar 2021</s> Ausgefallen wegen fehlender Teilnehmer!)
* <a href="https://tutorials-raspberrypi.de/raspberry-pi-rtc-modul-i2c-echtzeituhr/">Echtzeituhr</a> (<s>März 2021</s> Ausgefallen wegen fehlender Teilnehmer!)
* <a href="https://www.pcwelt.de/ratgeber/Elektronischer-Tueroeffner-im-Eigenbau-9965445.html">Türöffner</a> (April 2021)
* <a href="https://tutorials-raspberrypi.de/raspberry-pi-briefkasten-sensor-post-email-benachrichtig/">Briefkasten</a> (Mai 2021)
* <a href="https://tutorials-raspberrypi.de/automatisches-raspberry-pi-gewaechshaus-selber-bauen/">Gew&auml;chshaus</a> (August 2021)
* <a href="https://www.golem.de/news/mitmachprojekt-temperatur-messen-und-senden-mit-dem-raspberry-pi-1604-120188.html">Zugriff auf Temperatur-PI</a>
* <a href="http://www.donkeycar.com/">Donkey-Car</a>
* <a href="ledstrips.md">LED-Strip als bunte Beleuchtung</a>

Folgende Projekte wurden bereits vorgestellt:<br>

* <a href="robocar.md">Roboterauto</a> (<s>Juni 2021</s> Ausgefallen wegen fehlender Teilnehmer!)
* <a href="raspihandy.md">Raspi als Handy</a> (<s>Januar 2021</s> Ausgefallen wegen fehlender Teilnehmer!)
* <a href="jukebox.md">Digitale Jukebox</a> (<s>Dezember 2020</s> Ausgefallen wegen fehlender Teilnehmer!)
* <a href="keypad.md">Keypad auslesen</a> (am 17.11.2020 / leider wegen mangelnder Teilnehmerzahl ausgefallen.)
* <a href="lichtschranke.md">Lichtschranke</a> (am 20.10.2020)
* <a href="funksteckdose.md">Funksteckdose</a> (am 15.09.2020)
* <a href="rfidreader.md">RFID-Reader am Beispiel einer automatischen Tür</a> (am 25.08.2020)
* <a href="mwstumrech.md">Ein Webserver mit einer PHP-App für die MwSt.-Umrechnung</a> (am 21.07.2020)
* <a href="sms.md">SMS senden und empfangen</a> (am 16.06.2020)
* <a href="abstandmessen.md">Abstand messen mit Ultraschall</a> (am 27.05.2020 - Online-Meeting, Link siehe unter Termine)
* <a href="opentopics.md">Offener Themenabend</a> (am 21.04.2020 - Online-Meeting, Link siehe unter Termine)
* <a href="Giessomat.md">Giessomat</a> (März 2020 - wegen Corona leider ausgefallen)
* <a href="servomotor.md">Ansteuern eines Servomotors</a> (vorgestellt am 18.02.2020)
* <a href="voice-over-ip.md">Voice-over-IP Telefonie</a> (vorgestellt am 21.01.2020)
* <a href="Adventskranz.md">Adventskranz mit Relais-Schaltung</a> (vorgestellt am 11.12.2019)
* <a href="Webradio.md">Webradio</a> (war geplant f&uuml;r 13.11.2019)
* <a href="USB_Webcam.md">USB-Webcam ins Netzwerk einbinden</a> (vorgestellt am 16.10.2019)
* <a href="Wlan_Anbindung.md">Wlan-Anbindung</a> (vorgestellt am 17.09.2019)
* <a href="Motorsteuerung.md">Motorsteuerung</a> (vorgestellt am 20.08.2019)
* <a href="calibre.md">EBook-Reader</a> (vorgestellt am 11.06.2019)
* <a href="Roboterarm_mit_Webcam_ansteuern.md">Roboterarm und Webcam ferngesteuert</a> und <a href="projects/feuchtigkeitssensor.md">Feuchtigkeitssensor</a> (vorgestellt am 14.05.2019)
* <a href="KSR10_Roboterarm.md">Roboterarm</a> (vorgestellte am 16.04.2019)
* <a href="USB_Steckdosenleiste.md">USB-Steckdosenleiste</a> (vorgestellt am 19.03.2019)
* <a href="mqtt.md">MQTT</a> (vorgestellt am 12.02.2019)
